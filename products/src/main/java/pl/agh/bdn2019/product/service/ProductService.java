package pl.agh.bdn2019.product.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.product.assembler.ProductAssembler;
import pl.agh.bdn2019.product.dto.CreateOrUpdateProductDto;
import pl.agh.bdn2019.product.dto.ProductDto;
import pl.agh.bdn2019.product.model.Category;
import pl.agh.bdn2019.product.model.Product;
import pl.agh.bdn2019.product.model.Supplier;
import pl.agh.bdn2019.product.repository.CategoryRepository;
import pl.agh.bdn2019.product.repository.ProductRepository;
import pl.agh.bdn2019.product.repository.SupplierRepository;
import pl.agh.bdn2019.common.service.CrudService;

import java.util.List;

/**
 * Product service
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductService implements CrudService<Integer, ProductDto, CreateOrUpdateProductDto> {
    private ProductRepository repository;

    private SupplierRepository supplierRepository;

    private CategoryRepository categoryRepository;

    private ProductAssembler assembler;

    @Override
    public Integer create(CreateOrUpdateProductDto dto) {
        Supplier s = supplierRepository.getById(dto.getSupplier());
        Category c = categoryRepository.getById(dto.getCategory());
        Product product = Product.create(c,s,dto);
        return repository.save(product).getId();
    }

    @Override
    public void update(Integer integer, CreateOrUpdateProductDto dto) {
        Product product = repository.getById(integer);
        Supplier s = supplierRepository.getById(dto.getSupplier());
        Category c = categoryRepository.getById(dto.getCategory());
        product.update(c, s, dto);
    }

    @Override
    public void delete(Integer integer) {
        Product product = repository.getById(integer);
        repository.delete(product);
    }

    public boolean exists(Integer integer) {
        return repository.existsById(integer);
    }

    @Override
    public ProductDto get(Integer integer) {
        return assembler.toDto(repository.getById(integer));
    }

    @Override
    public List<ProductDto> getAll() {
        return assembler.toDtoList(repository.findAll());
    }
}
