CREATE SCHEMA Northwind;

CREATE TABLE IF NOT EXISTS Northwind.Customers
(
    CustomerID   SERIAL,
    CompanyName  varchar(40) default NULL,
    ContactName  varchar(30) default NULL,
    ContactTitle varchar(30) default NULL,
    Address      varchar(60) default NULL,
    City         varchar(15) default NULL,
    Region       varchar(15) default NULL,
    PostalCode   varchar(10) default NULL,
    Country      varchar(15) default NULL,
    Phone        varchar(24) default NULL,
    Fax          varchar(24) default NULL,
    PRIMARY KEY (CustomerID)
);

CREATE TABLE IF NOT EXISTS Northwind.CustomerDemographics
(
    CustomerTypeID SERIAL,
    CustomerDesc   VARCHAR(255),

    PRIMARY KEY (CustomerTypeID)
);

CREATE TABLE IF NOT EXISTS Northwind.CustomerCustomerDemo
(
    CustomerID     INTEGER NOT NULL,
    CustomerTypeID INTEGER NOT NULL,

    PRIMARY KEY (CustomerID, CustomerTypeID),
    FOREIGN KEY (CustomerID) REFERENCES Northwind.Customers (CustomerID),
    FOREIGN KEY (CustomerTypeID) REFERENCES Northwind.CustomerDemographics (CustomerTypeID)
);