package pl.agh.bdn2019.product.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.product.service.ProductFinderService;
import pl.agh.bdn2019.product.dto.ProductDto;
import pl.agh.bdn2019.product.dto.ProductSearchCriteriaDto;

import java.util.List;

@RestController
@RequestMapping("/search")
@Api(value = "Endpoint for searching products", tags = ApiDocumentationTags.PRODUCT)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductFinderController {
    private ProductFinderService service;

    @GetMapping
    @ApiOperation(value ="Search products", tags = ApiDocumentationTags.PRODUCT)
    public ResponseDto<List<ProductDto>> getSuppliers(@RequestBody ProductSearchCriteriaDto dto) {
        return ResponseDto.success(service.search(dto));
    }
}
