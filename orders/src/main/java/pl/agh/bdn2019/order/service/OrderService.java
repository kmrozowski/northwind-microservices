package pl.agh.bdn2019.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.common.service.CrudService;
import pl.agh.bdn2019.customer.client.CustomerClientService;
import pl.agh.bdn2019.customer.dto.CustomerDto;
import pl.agh.bdn2019.employee.client.EmployeeClientService;
import pl.agh.bdn2019.employee.dto.EmployeeDto;
import pl.agh.bdn2019.order.assembler.OrderAssembler;
import pl.agh.bdn2019.order.dto.CreateOrUpdateOrderDto;
import pl.agh.bdn2019.order.dto.OrderDto;
import pl.agh.bdn2019.order.model.Order;
import pl.agh.bdn2019.order.model.OrderBuilder;
import pl.agh.bdn2019.order.model.Shipper;
import pl.agh.bdn2019.order.repository.OrderRepository;
import pl.agh.bdn2019.order.repository.ShipperRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OrderService implements CrudService<Integer, OrderDto, CreateOrUpdateOrderDto> {

    @Autowired
    private ShipperRepository shipperRepostitory;

    @Autowired
    private OrderRepository repository;

    @Autowired
    private OrderAssembler assembler;

    @Autowired
    private EmployeeClientService employeeClientService;

    @Autowired
    private CustomerClientService customerClientService;


    @Override
    public Integer create(CreateOrUpdateOrderDto dto) {
        Shipper shipper = shipperRepostitory.getById(dto.getShipperId());

        Order order = OrderBuilder.builder()
                .customer(dto.getCustomerId())
                .freight(dto.getFreight())
                .positions(dto.getOrderDetails())
                .requiredDate(dto.getRequiredDate())
                .shipAddress(dto.getShipAddress())
                .shipper(shipper)
                .build();

        return repository.save(order).getId();
    }

    @Override
    public void update(Integer integer, CreateOrUpdateOrderDto dto) {
        //TODO
    }

    @Override
    public void delete(Integer integer) {
        repository.delete(repository.getById(integer));
    }

    @Override
    public OrderDto get(Integer integer) {
        Order order = repository.getById(integer);
        EmployeeDto employee = employeeClientService.get(order.getEmployeeId());
        CustomerDto customer = customerClientService.get(order.getCustomerId());
        return assembler.toDto(order, employee, customer);
    }

    @Override
    public List<OrderDto> getAll() {
        //TODO
        return assembler.toDtoList(repository.findAll());
    }
}
