package pl.agh.bdn2019.common.dto.exception.security;

public class UnsupportedTokenTypeException extends InvalidTokenRequestException {

    public UnsupportedTokenTypeException(String token) {
        super("UNSUPPORTED_TOKEN_TYPE", token);
    }
}
