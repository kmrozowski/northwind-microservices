package pl.agh.bdn2019.product.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data transfer object for creating or updating product category.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Create product category object")
public class CreateOrUpdateCategoryDto implements Serializable {
	
	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = -3637693987057035853L;

	@ApiModelProperty(notes = "Category name (Required)")
	@NotEmpty
	private String name;
	
	@ApiModelProperty(notes = "Category description (Optional)")
	private String description;
	
	@ApiModelProperty(notes = "Category image (Optional)")
	private byte[] image;
}
