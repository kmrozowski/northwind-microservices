package pl.agh.bdn2019.user.model;

public enum RoleName {
    USER,
    ADMIN
}
