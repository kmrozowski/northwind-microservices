package pl.agh.bdn2019.product.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.config.PreAuthorizeTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.product.dto.*;
import pl.agh.bdn2019.product.service.CategoryService;
import pl.agh.bdn2019.product.service.ProductService;
import pl.agh.bdn2019.product.service.SupplierService;

/**
 * Web controller for Product and Category entities.
 */
@RestController
@CrossOrigin
@RequestMapping()
@Api(value = "Endpoint for managing products", tags = ApiDocumentationTags.PRODUCT)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductController {

    private CategoryService categoryService;

    private SupplierService supplierService;

    private ProductService productService;



    @GetMapping("/{id}")
    @Secured("permitAll")
    @ApiOperation(value ="Gets product with specified id", tags = {ApiDocumentationTags.PRODUCT})
    public ResponseDto<ProductDto> get(@ApiParam("Id of the product") @PathVariable Integer id) {
        return ResponseDto.success(productService.get(id));
    }

    @GetMapping("/exists/{id}")
    @Secured("permitAll")
    @ApiOperation(value ="Checks if the product with specified id exists", tags = {ApiDocumentationTags.PRODUCT})
    public ResponseDto<Boolean> exists(@ApiParam("Id of the product") @PathVariable Integer id) {
        return ResponseDto.success(productService.exists(id));
    }

    @GetMapping()
    @Secured(PreAuthorizeTags.SEE_ALL_PRODUCTS)
    @ApiOperation(value ="Get all products", tags = {ApiDocumentationTags.PRODUCT})
    public ResponseDto<List<ProductDto>> getAll() {
        return ResponseDto.success(productService.getAll());
    }

    @PostMapping()
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Create product", tags = {ApiDocumentationTags.PRODUCT})
    public ResponseDto<Integer> create(@ApiParam("Data transfer object")
                                           @RequestBody CreateOrUpdateProductDto dto) {
        return ResponseDto.success(productService.create(dto));
    }

    @PutMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Updates product with specified id", tags = {ApiDocumentationTags.PRODUCT})
    public ResponseDto<Void> update(@ApiParam("Id of the product to be updated") @PathVariable Integer id,
                                            @ApiParam("Data transfer object")
                                            @RequestBody CreateOrUpdateProductDto dto) {
        productService.update(id, dto);
        return ResponseDto.success();
    }

    @DeleteMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Deletes product with specified id", tags = {ApiDocumentationTags.PRODUCT})
    public ResponseDto<Void> delete(@ApiParam("Id of the product to be deleted") @PathVariable Integer id) {
        productService.delete(id);
        return ResponseDto.success();
    }

    @PostMapping("/category")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value = "Creates new product category",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<Integer> createCategory(@ApiParam("Creation request")
                                                   @RequestBody CreateOrUpdateCategoryDto dto) {
        return ResponseDto.success(categoryService.create(dto));
    }

    @GetMapping("/category")
    @ApiOperation(value = "Get all product categories",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<List<CategoryDto>> getAllCategories() {
        return ResponseDto.success(categoryService.getAll());
    }

    @GetMapping("/category/{id}")
    @Secured("permitAll")
    @ApiOperation(value = "Get category with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<CategoryDto> getCategory(@ApiParam("Id of the category ") @PathVariable Integer id) {
        return ResponseDto.success(categoryService.get(id));
    }

    @PutMapping("/category/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Updates product category with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<Void> updateCategory(@ApiParam("Id of the category to be updated") @PathVariable Integer id,
                               @ApiParam("Data transfer object") @RequestBody CreateOrUpdateCategoryDto dto) {
        categoryService.update(id, dto);
        return ResponseDto.success();
    }

    @DeleteMapping("/category/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Deletes product category with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<Void> deleteCategory(@ApiParam("Id of the category to be updated") @PathVariable Integer id) {
        categoryService.delete(id);
        return ResponseDto.success();
    }

    @GetMapping("/supplier/{id}")
    @Secured("permitAll")
    @ApiOperation(value ="Gets supplier with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<SupplierDto> getSupplier(@ApiParam("Id of the supplier") @PathVariable Integer id) {
        return ResponseDto.success(supplierService.get(id));
    }

    @GetMapping("/supplier")
    @ApiOperation(value ="Get all suppliers",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<List<SupplierDto>> getSuppliers() {
        return ResponseDto.success(supplierService.getAll());
    }

    @PostMapping("/supplier")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Create supplier",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<Integer> createSupplier(@ApiParam("Data transfer object")
                                                   @RequestBody CreateOrUpdateSupplierRequestDto dto) {
        return ResponseDto.success(supplierService.create(dto));
    }

    @PutMapping("/supplier/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Updates supplier with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<Void> updateSupplier(@ApiParam("Id of the supplier to be updated") @PathVariable Integer id,
                               @ApiParam("Data transfer object")
                               @RequestBody CreateOrUpdateSupplierRequestDto dto) {
        supplierService.update(id, dto);
        return ResponseDto.success();
    }

    @DeleteMapping("/supplier/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Deletes supplier with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<Void> deleteSupplier(@ApiParam("Id of the supplier to be updated") @PathVariable Integer id) {
        supplierService.delete(id);
        return ResponseDto.success();
    }
}
