package pl.agh.bdn2019.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

/**
 * Data transfer object for order positions.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Order position")
public class OrderDetailsDto {

    @ApiModelProperty(notes = "Product id")
    @NotNull
    private Integer productId;

    @ApiModelProperty(notes = "Unit Price")
    @NotNull
    @Positive
    private BigDecimal unitPrice;

    @ApiModelProperty(notes = "Quantity")
    @NotNull
    @Positive
    private Integer quantity;

    @ApiModelProperty(notes = "Discount (absolute)")
    @PositiveOrZero
    private BigDecimal discount;
}
