package pl.agh.bdn2019.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.agh.bdn2019.customer.dto.exception.DemographicNotFoundException;
import pl.agh.bdn2019.customer.model.CustomerDemographic;

public interface CustomerDemographicRepository extends JpaRepository<CustomerDemographic, Integer> {

    /**
     * Gets customer demographic by id or throw exception if it does not exists.
     * @param id Entity id
     * @return product category
     */
    default CustomerDemographic getById(Integer id) {
        return findById(id).orElseThrow(() -> new DemographicNotFoundException(id));
    }
}
