package pl.agh.bdn2019.user.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;
import pl.agh.bdn2019.common.model.EntityWithID;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * User data entity
 */
@Entity
@Table(name = "Users")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name="UserID"))
})
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class User extends EntityWithID<Integer> {

    @NaturalId
    @Column(name = "Email", unique = true)
    @NotBlank(message = "User email cannot be null")
    private String email;

    @Column(name = "Username", unique = true)
    //@NullOrNotBlank(message = "Username can not be blank")
    private String username;

    @Column(name = "Password")
    @NotNull(message = "Password cannot be null")
    private String password;

    @Column(name = "FirstName")
   // @NullOrNotBlank(message = "First name can not be blank")
    private String firstName;

    @Column(name = "LastName")
   // @NullOrNotBlank(message = "Last name can not be blank")
    private String lastName;

    @Column(name = "Active", nullable = false)
    private Boolean active;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "UserAuthorities", joinColumns = {
            @JoinColumn(name = "UserID", referencedColumnName = "UserID")}, inverseJoinColumns = {
            @JoinColumn(name = "RoleID", referencedColumnName = "RoleID")})
    private Set<Role> roles = new HashSet<>();

    @Column(name = "EmailVerified", nullable = false)
    private Boolean isEmailVerified;

    User(User user) {
        setId(user.getId());
        username = user.getUsername();
        password = user.getPassword();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        email = user.getEmail();
        active = user.getActive();
        roles = user.getRoles();
        isEmailVerified = user.getIsEmailVerified();
    }
}
