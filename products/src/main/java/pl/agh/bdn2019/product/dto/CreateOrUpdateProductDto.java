package pl.agh.bdn2019.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Create or update product object")
public class CreateOrUpdateProductDto implements Serializable {

    @ApiModelProperty(notes = "Product name (Required)")
    @NotEmpty
    private String name;

    @ApiModelProperty(notes = "Supplier id")
    private Integer supplier;

    @ApiModelProperty(notes = "Category id")
    private Integer category;

    @ApiModelProperty(notes = "Quantity per unit")
    private String quantityPerUnit;

    @ApiModelProperty(notes = "Unit Price")
    private BigDecimal unitPrice;

    @ApiModelProperty(notes = "Units in stock")
    private Integer unitsInStock;

    @ApiModelProperty(notes = "Units in order")
    private Integer unitsOnOrder;

    @ApiModelProperty(notes = "Reorder level")
    private Integer reorderLevel;

    @ApiModelProperty(notes = "Is discontinued")
    private boolean discontinued;
}
