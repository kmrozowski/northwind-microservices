package pl.agh.bdn2019.employee.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Employee Territory")
public class TerritoryDto {
    private Integer id;
    private String description;

    private RegionDto region;
}
