package pl.agh.bdn2019.product.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.common.assembler.AddressAssembler;
import pl.agh.bdn2019.product.dto.SupplierDto;
import pl.agh.bdn2019.product.model.Supplier;


/**
 * Assembler for suppliers
 */
@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SupplierAssembler implements AbstractAssembler<Supplier, SupplierDto> {
	private AddressAssembler addressAssembler;
	
	@Override
	public SupplierDto toDto(Supplier entity) {
		SupplierDto out = new SupplierDto();
		out.setAddress(addressAssembler.toDto(entity.getAddress()));
		out.setCompanyName(entity.getCompanyName());
		out.setContactName(entity.getCompanyName());
		out.setContactTitle(entity.getContactName());
		out.setFax(entity.getFax());
		out.setHomepage(entity.getHomepage());
		out.setId(entity.getId());
		out.setPhone(entity.getPhone());
		return out;
	}

}
