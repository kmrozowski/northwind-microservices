package pl.agh.bdn2019.customer.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.agh.bdn2019.common.model.EntityWithID;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Customer demographic entity
 */
@Entity
@Table(name = "CustomerDemographics")
@AttributeOverride(name = "id", column = @Column(name="CustomerTypeID"))
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomerDemographic extends EntityWithID<Integer> {

    @Column(name = "CustomerDesc")
    private String description;

    public static CustomerDemographic create(String description){
        return new CustomerDemographic(description);
    }

    public void update(String description) {
        this.description = description;
    }
}
