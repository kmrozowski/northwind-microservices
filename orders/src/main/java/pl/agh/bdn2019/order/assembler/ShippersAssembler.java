package pl.agh.bdn2019.order.assembler;

import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.order.dto.ShipperDto;
import pl.agh.bdn2019.order.model.Shipper;

/**
 * Assembler for shippers
 */
@Component
public class ShippersAssembler implements AbstractAssembler<Shipper, ShipperDto> {
    @Override
    public ShipperDto toDto(Shipper entity) {
        return new ShipperDto(entity.getId(), entity.getCompanyName(), entity.getPhone());
    }
}
