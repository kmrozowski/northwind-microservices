package pl.agh.bdn2019.common.aop;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.common.dto.exception.ApplicationException;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Controller advice for REST controllers. Catches exceptions and translates their error messages
 */
@RestControllerAdvice
@Slf4j
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuthenticationControllerAdvice {

    private MessageSource messageSource;

    private String resolveMessage(String code, Object[] args) {
        Locale currentLocale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(code, args, currentLocale);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResponseDto<?> handleNotFoundException(NoHandlerFoundException ex, WebRequest req) {
        String message = resolveMessage("URL_NOT_FOUND", new Object[]{});
        return ResponseDto.error("URL_NOT_FOUND", message);
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ResponseDto<?> handleUnauthorizedException(AuthenticationException ex, WebRequest request) {
        String message = resolveMessage("UNAUTHORIZED_ACCESS", new Object[]{});
        log.error("Unauthorized access: ", ex);
        return ResponseDto.error("UNAUTHORIZED_ACCESS", message);
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ResponseDto<?> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
        String message = resolveMessage("ACCESS_DENIED", new Object[]{});
        return ResponseDto.error("ACCESS_DENIED", message);
    }

    @ExceptionHandler(value = UsernameNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResponseDto<?> handleUsernameNotFoundException(UsernameNotFoundException ex, WebRequest request) {
        String message = resolveMessage("USERNAME_NOT_FOUND", new Object[]{});
        return ResponseDto.error("USERNAME_NOT_FOUND", message);
    }

    @ExceptionHandler(value = BadCredentialsException.class)
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    @ResponseBody
    public ResponseDto<?> handleBadCredentialsException(BadCredentialsException ex, WebRequest request) {
        String message = resolveMessage("INVALID_CREDENTIALS", new Object[]{});
        log.error("Invalid credentials: ", ex);
        return ResponseDto.error("INVALID_CREDENTIALS", message);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseDto<?> handleValidationException(MethodArgumentNotValidException ex, WebRequest r) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(e -> {
            String field = ((FieldError) e).getField();
            String message = e.getDefaultMessage();
            errors.put(field, message);
        });
        String message = resolveMessage("VALIDATION_FAILED", new Object[]{});
        return ResponseDto.validationError("VALIDATION_FAILED", message, errors);
    }


    @ExceptionHandler(ApplicationException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseDto<?> handleApplicationException(ApplicationException ex, WebRequest request) {
        String message = resolveMessage(ex.getCode(), ex.getArgs());
        return ResponseDto.error(ex.getCode(), message);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseDto<?> handleException(Exception ex, WebRequest r) {
        String message = resolveMessage("INTERNAL_ERROR", new Object[]{});
        log.error("Unknown error:", ex);
        return ResponseDto.error("INTERNAL_ERROR", message);
    }
}
