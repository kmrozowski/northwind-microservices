package pl.agh.bdn2019.common.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;
import pl.agh.bdn2019.common.dto.exception.security.InvalidatedTokenException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class TokenAuthenticationFilter extends BasicAuthenticationFilter {
    @Value("${northwind.security.header}")
    private String tokenRequestHeader;
    @Value("${northwind.security.header.prefix}")
    private String tokenRequestHeaderPrefix;

    @Autowired
    private TokenService service;

    @Autowired
    private RedisUserDetailsService userService;

    @Autowired
    public TokenAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    public void doFilterInternal(HttpServletRequest request,
                                 HttpServletResponse response,
                                 FilterChain chain) throws IOException, ServletException {
        if (!request.getMethod().equals("OPTIONS")) {
            try {
                String bearerToken = request.getHeader("Authorization");
                if (bearerToken == null || !bearerToken.startsWith("Bearer ")) {
                    SecurityContextHolder.getContext().setAuthentication(null);
                    chain.doFilter(request, response);
                    return;
                }
                String jwt = bearerToken.substring(7);
                if (StringUtils.hasText(jwt) && service.validateToken(jwt)) {
                    if (!service.tokenExists(jwt)) {
                        throw new InvalidatedTokenException();
                    }
                    Integer userId = service.getUserIdFromJWT(jwt);
                    UserDetails user = userService.loadUserByToken(jwt);
                    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                    token.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(token);
                }
            } catch (AuthenticationException e) {
                SecurityContextHolder.clearContext();
                throw e;
            }
        }
        chain.doFilter(request, response);
    }
}
