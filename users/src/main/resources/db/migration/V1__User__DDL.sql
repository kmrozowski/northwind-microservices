CREATE SCHEMA Northwind;

CREATE TABLE IF NOT EXISTS Northwind.Roles
(
    RoleID   SERIAL,

    RoleName VARCHAR(255),

    PRIMARY KEY (RoleID)
);

CREATE TABLE IF NOT EXISTS Northwind.Users
(
    UserID        SERIAL,

    Email         VARCHAR(255),
    Username      VARCHAR(255),
    Password      VARCHAR(255),
    FirstName     VARCHAR(255),
    LastName      VARCHAR(255),
    Active        BOOLEAN,
    EmailVerified BOOLEAN,

    PRIMARY KEY (UserID)
);

CREATE TABLE IF NOT EXISTS Northwind.UserAuthorities
(
    UserID INTEGER NOT NULL,
    RoleID INTEGER NOT NULL,

    PRIMARY KEY (UserID, RoleID),
    FOREIGN KEY (UserID) REFERENCES Northwind.Users (UserID),
    FOREIGN KEY (RoleID) REFERENCES Northwind.Roles (RoleID)
);