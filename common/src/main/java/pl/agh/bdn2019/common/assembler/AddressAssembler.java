package pl.agh.bdn2019.common.assembler;

import org.springframework.stereotype.Component;

import pl.agh.bdn2019.common.dto.AddressDto;
import pl.agh.bdn2019.common.types.Address;

/**
 * Assembler for Address value type
 */
@Component
public class AddressAssembler implements AbstractAssembler<Address, AddressDto> {

	@Override
	public AddressDto toDto(Address entity) {
		AddressDto dto = new AddressDto();
		dto.setAddress(entity.getAddress());
		dto.setCity(entity.getCity());
		dto.setCountry(entity.getCountry());
		dto.setPostalCode(entity.getPostalCode());
		dto.setRegion(entity.getRegion());
		return dto;
	}

}
