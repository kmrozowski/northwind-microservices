package pl.agh.bdn2019.common.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configuration class for all beans related to documentation.
 */
@Configuration
@EnableSwagger2
public class DocumentationConfiguration {

	@Autowired
	private Environment env;

	/**
	 * Docket config. Document every @RestController class.
	 * @return Docket configuration
	 */
	@Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(createApiInfo());
    }
	
	/**
	 * API info configuration. These are values used by swagger to display project info
	 * @return Api info containing necessary information regarding project
	 */
	private ApiInfo createApiInfo() {
		return new ApiInfo(env.getProperty("spring.application.name"),
				env.getProperty("northwind.description"),
				env.getProperty("northwind.version"),
				"", 
				new Contact(env.getProperty("northwind.team.members"), env.getProperty("northwind.team.url"), env.getProperty("northwind.team.emails")),
				"Apache License, v2.0",
				"See License.md file",
				Collections.emptyList());
	}
}
