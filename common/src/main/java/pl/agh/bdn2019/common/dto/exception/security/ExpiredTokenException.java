package pl.agh.bdn2019.common.dto.exception.security;

public class ExpiredTokenException extends InvalidTokenRequestException {

    public ExpiredTokenException(String token) {
        super("TOKEN_EXPIRED", token);
    }
}
