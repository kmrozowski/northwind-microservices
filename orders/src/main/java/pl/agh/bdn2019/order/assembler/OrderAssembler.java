package pl.agh.bdn2019.order.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.common.assembler.AddressAssembler;
import pl.agh.bdn2019.customer.dto.CustomerDto;
import pl.agh.bdn2019.employee.dto.EmployeeDto;
import pl.agh.bdn2019.order.dto.OrderDto;
import pl.agh.bdn2019.order.model.Order;


/**
 * Assembler for orders.
 */
@Component
public class OrderAssembler implements AbstractAssembler<Order, OrderDto> {

    @Autowired
    private OrderDetailsAssembler detailsAssembler;

    @Autowired
    private AddressAssembler addressAssembler;

    @Autowired
    private ShippersAssembler shipperAssembler;

    @Override
    public OrderDto toDto(Order entity) {
        OrderDto dto = new OrderDto();
        dto.setFreight(entity.getFreight());
        dto.setOrderDateTime(entity.getOrderDate());
        dto.setOrderDetails(detailsAssembler.toDtoSet(entity.getOrderDetails()));
        dto.setRequiredDate(entity.getRequiredDate());
        dto.setShipAddress(addressAssembler.toDto(entity.getShipAddress()));
        dto.setShippedDate(entity.getShippedDate());
        dto.setShipper(shipperAssembler.toDto(entity.getShipper()));
        return dto;
    }

    public OrderDto toDto(Order entity, EmployeeDto employee, CustomerDto customer) {
        OrderDto dto = toDto(entity);
        dto.setCustomer(customer);
        dto.setEmployee(employee);
        return dto;
    }
}
