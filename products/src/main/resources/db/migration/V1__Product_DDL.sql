CREATE SCHEMA IF NOT EXISTS Northwind;

/* SUPPLIERS */
CREATE TABLE IF NOT EXISTS Northwind.Suppliers (
  SupplierID      SERIAL,
  CompanyName     VARCHAR(50) NOT NULL,
  ContactName     VARCHAR(50) ,
  ContactTitle    VARCHAR(50) ,
  Address         VARCHAR(250) ,
  City            VARCHAR(50) ,
  Region          VARCHAR(50) ,
  PostalCode      VARCHAR(15) ,
  Country         VARCHAR(50) ,
  Phone           VARCHAR(25) ,
  Fax             VARCHAR(25) ,
  HomePage        VARCHAR(250) ,
  PRIMARY KEY (SupplierID)
);


/* CATEGORIES */
CREATE TABLE IF NOT EXISTS Northwind.Categories (
  CategoryID      SERIAL,
  CategoryName    VARCHAR(50) NOT NULL,
  Description     VARCHAR(250),
  Picture         bytea,
  PRIMARY KEY (CategoryID)
);

/* PRODUCTS */

CREATE TABLE IF NOT EXISTS Northwind.Products (
  ProductID       SERIAL,
  ProductName     varchar(40),
  SupplierID      INT REFERENCES northwind.suppliers(SupplierID),
  CategoryID      INT REFERENCES northwind.categories(CategoryID),
  QuantityPerUnit varchar(20) ,
  UnitPrice       NUMERIC(1,0),
  UnitsInStock    INT,
  UnitsOnOrder    INT,
  ReorderLevel    INT,
  Discontinued    BOOLEAN,

  PRIMARY KEY  (ProductID)
)