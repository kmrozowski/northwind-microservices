CREATE TABLE IF NOT EXISTS Northwind.Region
(
    RegionID          SERIAL,
    RegionDescription VARCHAR(255),

    PRIMARY KEY (RegionID)
);

CREATE TABLE IF NOT EXISTS Northwind.Territories
(
    TerritoryID          SERIAL,
    TerritoryDescription VARCHAR(255),
    RegionID             INTEGER NOT NULL,

    PRIMARY KEY (TerritoryID),
    FOREIGN KEY (RegionID) REFERENCES Northwind.Region (RegionID)
);

CREATE TABLE IF NOT EXISTS northwind.EmployeeTerritories
(
    EmployeeID  INTEGER NOT NULL,
    TerritoryID INTEGER NOT NULL,

    PRIMARY KEY (EmployeeID, TerritoryID),
    FOREIGN KEY (EmployeeID) REFERENCES Northwind.Employees (EmployeeID),
    FOREIGN KEY (TerritoryID) REFERENCES Northwind.Territories (TerritoryID)
);