package pl.agh.bdn2019.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.agh.bdn2019.common.dto.AddressDto;

import java.util.Set;

/**
 * Data transfer object for displaying customer.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel("Customer details")
public class CustomerDto {

    @ApiModelProperty("Database id")
    private Integer id;

    @ApiModelProperty("Company name")
    private String companyName;

    @ApiModelProperty("Contact name")
    private String contactName;

    @ApiModelProperty("Contact title")
    private String contactTitle;

    @ApiModelProperty("Address")
    private AddressDto address;

    @ApiModelProperty("Demographics")
    Set<CustomerDemographicDto> demographics;
}
