package pl.agh.bdn2019.product.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.config.PreAuthorizeTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.product.dto.CategoryDto;
import pl.agh.bdn2019.product.dto.CreateOrUpdateCategoryDto;
import pl.agh.bdn2019.product.service.CategoryService;

import java.util.List;

/**
 * Web controller for categories.
 */
@RestController
@CrossOrigin
@RequestMapping("/category")
@Api(value = "Endpoint for managing supplier", tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryController {

    private CategoryService categoryService;

    @PostMapping
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value = "Creates new product category",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<Integer> create(@ApiParam("Creation request")
                                       @RequestBody CreateOrUpdateCategoryDto dto) {
        return ResponseDto.success(categoryService.create(dto));
    }

    @GetMapping
    @Secured("permitAll")
    @ApiOperation(value = "Get all product categories",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<List<CategoryDto>> getAll() {
        return ResponseDto.success(categoryService.getAll());
    }

    @GetMapping("/{id}")
    @Secured("permitAll")
    @ApiOperation(value = "Get category with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<CategoryDto> get(@ApiParam(value = "Id of the category", example = "123") @PathVariable Integer id) {
        return ResponseDto.success(categoryService.get(id));
    }

    @PutMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value = "Updates product category with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<Void> update(@ApiParam(value = "Id of the category to be updated", example = "123") @PathVariable Integer id,
                                    @ApiParam("Data transfer object") @RequestBody CreateOrUpdateCategoryDto dto) {
        categoryService.update(id, dto);
        return ResponseDto.success();
    }

    @DeleteMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value = "Deletes product category with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.CATEGORIES})
    public ResponseDto<Void> delete(@ApiParam(value = "Id of the category to be updated", example = "123") @PathVariable Integer id) {
        //TODO remove only if there's no products with specified category
        categoryService.delete(id);
        return ResponseDto.success();
    }
}
