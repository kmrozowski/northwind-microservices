package pl.agh.bdn2019.product.dto.exception;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

/**
 *  Exception thrown when product with specified id is not found.
 */
public class ProductNotFoundException extends ApplicationException {

    public ProductNotFoundException(Integer id) {
        super("PRODUCT_NOT_FOUND", new Object[]{id});
    }
}
