package pl.agh.bdn2019.product.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.product.dto.CreateOrUpdateProductDto;
import pl.agh.bdn2019.product.dto.ProductDto;

import java.util.List;

@FeignClient("products")
public interface ProductClient {

    @GetMapping("/exists/{id}")
    ResponseDto<Boolean> exists(@PathVariable Integer id);

    @GetMapping("/{id}")
    ResponseDto<ProductDto> get(@PathVariable Integer id);

    @GetMapping()
    ResponseDto<List<ProductDto>> getAll();

    @PostMapping()
    ResponseDto<Integer> create(@RequestBody CreateOrUpdateProductDto dto);

    @PutMapping("/{id}")
    ResponseDto<Void> update(@PathVariable Integer id, @RequestBody CreateOrUpdateProductDto dto);

    @DeleteMapping("/{id}")
    ResponseDto<Void> delete(@PathVariable Integer id);
}
