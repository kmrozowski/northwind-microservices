package pl.agh.bdn2019.common.types;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Value type for address.
 */
@Embeddable
@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Address {

	/**
	 * Address - street with number, house number in villages.
	 */
	@Column(name = "address")
	private String address;

	/**
	 * City or village.
	 */
	@Column(name = "city")
	private String city;

	/**
	 * Country's region
	 */
	@Column(name = "region")
	private String region;

	/**
	 * Postal code. No validation is performed
	 */
	@Column(name = "postalCode")
	private String postalCode;

	/**
	 * Country.
	 */
	@Column(name = "country")
	private String country;
	
}
