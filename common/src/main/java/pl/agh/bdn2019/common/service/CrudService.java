package pl.agh.bdn2019.common.service;

import java.util.List;

/**
 * Basic CRUD service interface
 * @param <ID> id class
 * @param <T> Dto class
 * @param <D> Create or update class
 */
public interface CrudService<ID, T, D> {

	/**
	 * Create entity and return it's generated id
	 * @param dto Create or update dto
	 * @return Newly generated id
	 */
	ID create(D dto);

	/**
	 * Updates existing entity
	 * @param id id of the entity
	 * @param dto Create or update dto
	 */
	void update(ID id, D dto);

	/**
	 * Deletes entity with specified id
	 * @param id id of the entity
	 */
	void delete(ID id);

	/**
	 * Gets entity details
	 * @param id id of the entity
	 * @return Dto
	 */
	T get(ID id);

	/**
	 * Gets all entities
	 * @return List of DTOs
	 */
	List<T> getAll();
}
