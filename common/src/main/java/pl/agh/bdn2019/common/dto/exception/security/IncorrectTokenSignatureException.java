package pl.agh.bdn2019.common.dto.exception.security;

public class IncorrectTokenSignatureException extends InvalidTokenRequestException {

    public IncorrectTokenSignatureException(String token) {
        super("INVALID_SIGNATURE", token);
    }
}
