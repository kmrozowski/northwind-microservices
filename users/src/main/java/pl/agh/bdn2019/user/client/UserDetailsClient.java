package pl.agh.bdn2019.user.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.agh.bdn2019.common.dto.ResponseDto;

@FeignClient("user-details-client")
public interface UserDetailsClient {

    @GetMapping("/{id}")
    ResponseDto<UserDetails> getUser(@PathVariable Integer id);
}
