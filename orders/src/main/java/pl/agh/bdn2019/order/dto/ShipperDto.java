package pl.agh.bdn2019.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Data transfer object for order shipper.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Order shipper object")
public class ShipperDto {

    @ApiModelProperty(notes = "Shipper id")
    @NotNull
    private Integer shipperId;

    @ApiModelProperty(notes = "Shipper name")
    @NotEmpty
    private String companyName;

    @ApiModelProperty(notes = "Shipper phone")
    private String phone;
}
