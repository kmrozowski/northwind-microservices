package pl.agh.bdn2019.common.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing address
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Address data transfer object")
public class AddressDto implements Serializable {

	/**
	 * Address - street with number, house number in villages.
	 */
	@ApiModelProperty(notes = "Address(Required)")
	@NotEmpty
	private String address;

	/**
	 * City or village.
	 */
	@ApiModelProperty(notes = "City (Optional)")
	private String city;

	/**
	 * Country's region
	 */
	@ApiModelProperty(notes = "Region (Optional)")
	private String region;

	/**
	 * Postal code. No validation is performed
	 */
	@ApiModelProperty(notes = "Postal Code (Optional)")
	private String postalCode;

	/**
	 * Country.
	 */
	@ApiModelProperty(notes = "Country(Optional)")
	private String country;
}
