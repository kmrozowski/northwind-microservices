package pl.agh.bdn2019.employee.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.employee.dto.TerritoryDto;
import pl.agh.bdn2019.employee.model.Territory;

@Component
public class TerritoryAssembler implements AbstractAssembler<Territory, TerritoryDto> {
    @Autowired
    private RegionAssembler regionAssembler;
    @Override
    public TerritoryDto toDto(Territory entity) {
        return new TerritoryDto(entity.getId(), entity.getDescription(), regionAssembler.toDto(entity.getRegion()));
    }
}
