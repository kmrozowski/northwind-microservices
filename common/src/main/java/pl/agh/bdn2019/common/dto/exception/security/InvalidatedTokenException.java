package pl.agh.bdn2019.common.dto.exception.security;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

public class InvalidatedTokenException extends ApplicationException {

    public InvalidatedTokenException() {
        super("TOKEN_INVALIDATED", new Object[]{});
    }
}
