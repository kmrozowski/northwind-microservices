package pl.agh.bdn2019.product.assembler;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.product.dto.ProductDto;
import pl.agh.bdn2019.product.model.Product;

/**
 * Assembler for products
 */
@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductAssembler implements AbstractAssembler<Product, ProductDto> {

    private SupplierAssembler supplierAssembler;

    private CategoryAssembler categoryAssembler;

    @Override
    public ProductDto toDto(Product entity) {
        ProductDto dto = new ProductDto();
        dto.setCategory(categoryAssembler.toDto(entity.getCategory()));
        dto.setSupplier(supplierAssembler.toDto(entity.getSupplier()));
        dto.setDiscontinued(entity.isDiscontinued());
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setQuantityPerUnit(entity.getQuantityPerUnit());
        dto.setReorderLevel(entity.getReorderLevel());
        dto.setUnitPrice(entity.getUnitPrice());
        dto.setUnitsInStock(entity.getUnitsInStock());
        dto.setUnitsOnOrder(entity.getUnitsOnOrder());
        return dto;
    }
}
