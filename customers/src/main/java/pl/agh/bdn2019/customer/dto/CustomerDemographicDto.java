package pl.agh.bdn2019.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * Data transfer object for customer demographic.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel("Customer demographics")
public class CustomerDemographicDto {

    @ApiModelProperty("Demographic id")
    @NotEmpty
    private Integer id;

    @ApiModelProperty("Demographic description")
    @NotEmpty
    private String description;
}
