package pl.agh.bdn2019.common.dto.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.agh.bdn2019.common.dto.ErrorDto;

@AllArgsConstructor
@Getter
public class ClientBusinessException extends RuntimeException {
    private ErrorDto errorDto;

}
