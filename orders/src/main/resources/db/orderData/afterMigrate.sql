-- noinspection SpellCheckingInspectionForFile

/*
	This file is run after every migration, so make sure it can be executed multiple times.
	Data source (For MSSQL Server): https://raw.githubusercontent.com/microsoft/sql-server-samples/master/samples/databases/northwind-pubs/instnwnd.sql
*/


DELETE
FROM northwind.shippers;


INSERT INTO northwind.shippers (ShipperID, CompanyName, Phone)
VALUES (1, 'Speedy Express', '(503) 555-9831'),
       (2, 'United Package', '(503) 555-3199'),
       (3, 'Federal Shipping', '(503) 555-9931');
