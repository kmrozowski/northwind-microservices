package pl.agh.bdn2019.common.service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Abstract class for all finders (services that search for entities)
 */
public abstract class AbstractFinder<T, D> {
    /**
     * Search for entities and return list of DTOs
     * @param criteriaDto criteria for search. Null in field means value will not be searched against
     * @return List of all entities that match specified criteria
     */
    public abstract List<D> search(T criteriaDto);

    public void addBoolean(BooleanPath p, BooleanBuilder bb, Boolean field) {
        if(field != null) {
            bb.and(p.eq(field));
        }
    }
    /**
     * Search numbers equal to
     * @param p
     * @param bb
     * @param field
     */
    public void addEquals(NumberPath p, BooleanBuilder bb, Number field) {
        if(!StringUtils.isEmpty(field)) {
            bb.and(p.eq(field));
        }
    }

    /**
     * Search numbers greater or equal to
     * @param p
     * @param bb
     * @param field
     */
    public void addGreaterOrEqual(NumberPath p, BooleanBuilder bb, Number field) {
        if(!StringUtils.isEmpty(field)) {
            bb.and(p.goe(field));
        }
    }

    /**
     * Search numbers less or equal to
     * @param p
     * @param bb
     * @param field
     */
    public void addLesserOrEqual(NumberPath p, BooleanBuilder bb, Number field) {
        if(!StringUtils.isEmpty(field)) {
            bb.and(p.loe(field));
        }
    }

    /**
     * Search strings (case insensitive) when criteria is not empty
     * @param p
     * @param bb
     * @param field
     */
    public void addNonEmpty(StringPath p, BooleanBuilder bb, String field) {
        if(!StringUtils.isEmpty(field)) {
            bb.and(p.equalsIgnoreCase(field));
        }
    }
    /**
     * Search strings starting with text (case insensitive) when criteria is not empty
     * @param p
     * @param bb
     * @param field
     */
    public void addNonEmptyLike(StringPath p, BooleanBuilder bb, String field) {
        if(!StringUtils.isEmpty(field)) {
            bb.and(p.equalsIgnoreCase(field));
        }
    }
}
