package pl.agh.bdn2019.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.agh.bdn2019.common.dto.AddressDto;

import java.util.Set;

/**
 * Data transfer object for creating or updating customer.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel("Create or update customer object")
public class CreateOrUpdateCustomerDto {

    @ApiModelProperty("Company name")
    private String companyName;

    @ApiModelProperty("Contact name")
    private String contactName;

    @ApiModelProperty("Contact title")
    private String contactTitle;

    @ApiModelProperty("Contact title")
    private AddressDto address;

    @ApiModelProperty("Demographics")
    private Set<Integer> demographics;
}
