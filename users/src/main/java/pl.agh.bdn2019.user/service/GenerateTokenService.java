package pl.agh.bdn2019.user.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.common.security.RedisService;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.time.Instant;
import java.util.Date;

@Service
public class GenerateTokenService {
    @Value("${northwind.security.secret}")
    private String jwtSecret;

    @Value("${northwind.security.expiration}")
    @Getter
    private Long jwtExpirationInMs;

    /*
       @Value("${app.jwt.claims.refresh.name}")
       private String jwtClaimRefreshName;
   */

    @Autowired
    private RedisService redisService;

    @Autowired
    private UserService userService;

    public String generateToken(Integer userID) {
        Instant expiryDate = Instant.now().plusMillis(jwtExpirationInMs);
        String userId = Long.toString(userID);
        String token = Jwts.builder()
                .setSubject(userId)
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(expiryDate))
                .signWith(getSigningKey())
                .compact();
        UserDetails ud = userService.loadUserById(userID);
        redisService.add(token, ud);
        return token;
    }

    public void invalidateToken(String authToken) {
        String header = authToken;
        if (header.startsWith("Bearer")) {
            header = header.substring(7);
        }
        redisService.delete(header);
    }

    private Key getSigningKey() {
        byte[] keyBytes = jwtSecret.getBytes(StandardCharsets.UTF_8);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
