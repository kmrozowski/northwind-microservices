package pl.agh.bdn2019.order.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.agh.bdn2019.common.dto.AddressDto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

/**
 * Data transfer object for creating or updating order.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Order position")
public class CreateOrUpdateOrderDto {
    @ApiModelProperty("Customer id")
    private Integer customerId;

    @ApiModelProperty("Order details, must be null in update mode")
    private Set<OrderDetailsDto> orderDetails;

    @ApiModelProperty("Required date")
    private LocalDate requiredDate;

    @ApiModelProperty("Freight")
    private BigDecimal freight;

    @ApiModelProperty("Shipment Address")
    private AddressDto shipAddress;

    @ApiModelProperty("Shipper id")
    private Integer shipperId;
}
