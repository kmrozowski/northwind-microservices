package pl.agh.bdn2019.order.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Order shipper")
public class CreateOrUpdateShipperDto {
    private String companyName;
    private String phone;
}
