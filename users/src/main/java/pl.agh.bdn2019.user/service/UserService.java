package pl.agh.bdn2019.user.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.user.assembler.UserInfoAssembler;
import pl.agh.bdn2019.user.dto.UserInfoDto;
import pl.agh.bdn2019.user.dto.exception.UserDoesNotExistsException;
import pl.agh.bdn2019.user.model.JwtUserDetails;
import pl.agh.bdn2019.user.model.User;
import pl.agh.bdn2019.user.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

    private UserRepository userRepository;

    private UserInfoAssembler assembler;
    /**
     * Get current (logged-in) user information.
     * @return Currently logged user dto (without password field)
     */
    public UserInfoDto getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JwtUserDetails details = (JwtUserDetails) auth.getPrincipal();
        User user = userRepository.findByEmail(details.getEmail())
                .orElseThrow(() -> new UserDoesNotExistsException(details.getUsername()));

        return assembler.toDto(user);
    }

    /**
     * Get user by it's id
     * @param Id User id (as used in the database)
     * @return Optional object containing user if record in the database exists
     */
    public Optional<User> findById(Integer Id) {
        return userRepository.findById(Id);
    }


    /**
     * Check is the user exists given the email: naturalId
     */
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    /**
     * Check is the user exists given the username: naturalId
     */
    public Boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public UserDetails loadUserById(Integer id) {
        Optional<User> dbUser = userRepository.findById(id);
        return dbUser.map(JwtUserDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("Couldn't find a matching user id in the database for " + id));
    }

}
