package pl.agh.bdn2019.product.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.agh.bdn2019.product.dto.exception.SupplierNotFoundException;
import pl.agh.bdn2019.product.model.Supplier;


/**
 * Supplier repository
 */
public interface SupplierRepository extends PagingAndSortingRepository<Supplier, Integer> {

	/**
	 * Gets supplier by id or throw exception if it does not exists.
	 * @param id Entity id
	 * @return supplier
	 */
	default Supplier getById(Integer id) {
		return findById(id).orElseThrow(() -> new SupplierNotFoundException(id));
	}
	
}
