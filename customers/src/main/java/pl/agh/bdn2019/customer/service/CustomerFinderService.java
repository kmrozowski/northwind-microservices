package pl.agh.bdn2019.customer.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.common.service.AbstractFinder;
import pl.agh.bdn2019.customer.assembler.CustomerAssembler;
import pl.agh.bdn2019.customer.dto.CustomerDto;
import pl.agh.bdn2019.customer.dto.SearchCustomerCriteriaDto;
import pl.agh.bdn2019.customer.model.QCustomer;
import pl.agh.bdn2019.customer.repository.CustomerFinder;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

/**
 * Service for searching customers
 */
@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CustomerFinderService extends AbstractFinder<SearchCustomerCriteriaDto, CustomerDto> {

    private CustomerFinder finder;

    private CustomerAssembler assembler;

    @Override
    public List<CustomerDto> search(SearchCustomerCriteriaDto criteriaDto) {
        QCustomer q = QCustomer.customer;
        BooleanBuilder bb = new BooleanBuilder();
        addNonEmpty(q.companyName, bb, criteriaDto.getCompanyName());
        addNonEmpty(q.contactName, bb, criteriaDto.getContactName());
        addNonEmpty(q.address.address, bb, criteriaDto.getAddress());
        addNonEmpty(q.address.city, bb, criteriaDto.getCity());
        addNonEmpty(q.address.region, bb, criteriaDto.getRegion());
        addNonEmpty(q.address.postalCode, bb, criteriaDto.getPostalCode());
        addNonEmpty(q.address.country, bb, criteriaDto.getCountry());

        return assembler.toDtoList(finder.findAll(Objects.requireNonNull(bb.getValue())));
    }
}
