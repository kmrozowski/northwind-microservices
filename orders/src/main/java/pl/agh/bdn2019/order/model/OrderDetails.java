package pl.agh.bdn2019.order.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Product entity
 */
@Entity
@Table(name = "OrderDetails")
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class OrderDetails implements Serializable {

    @EmbeddedId
    private OrderDetailsId id;

    @ManyToOne
    @JoinColumn(name = "OrderID")
    private Order order;

    @Column(name = "ProductID")
    private Integer productId;

    @Column(name = "UnitPrice")
    @Positive
    private BigDecimal unitPrice;

    @Column(name = "Quantity")
    @Positive
    private Integer quantity;

    @Column(name = "Discount")
    @PositiveOrZero
    private BigDecimal discount;

    OrderDetails(Order order, Integer productId, @Positive BigDecimal unitPrice, @Positive Integer quantity, @PositiveOrZero BigDecimal discount) {
        this.order = order;
        this.productId = productId;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
        this.discount = discount;
    }

    /**
     * Get total price of this order position.
     * @return
     */
    public BigDecimal totalPrice() {
        return unitPrice.multiply(BigDecimal.valueOf(quantity)).subtract(discount);
    }
}
