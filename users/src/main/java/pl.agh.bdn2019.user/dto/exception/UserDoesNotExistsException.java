package pl.agh.bdn2019.user.dto.exception;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

public class UserDoesNotExistsException extends ApplicationException {

    public UserDoesNotExistsException(String user) {
        super("USER_DOES_NOT_EXIST", new Object[]{});
    }
}
