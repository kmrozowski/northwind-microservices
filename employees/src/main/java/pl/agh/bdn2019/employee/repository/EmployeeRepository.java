package pl.agh.bdn2019.employee.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.agh.bdn2019.employee.dto.exception.EmployeeNotFoundException;
import pl.agh.bdn2019.employee.model.Employee;

public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Integer> {
    /**
     * Gets product by id or throw exception if it does not exists.
     * @param id Entity id
     * @return product
     */
    default Employee getById(Integer id) {
        return findById(id).orElseThrow(() -> new EmployeeNotFoundException(id));
    }
}
