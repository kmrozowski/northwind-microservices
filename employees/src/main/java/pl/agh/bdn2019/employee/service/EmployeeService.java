package pl.agh.bdn2019.employee.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.common.service.CrudService;
import pl.agh.bdn2019.employee.assembler.EmployeeAssembler;
import pl.agh.bdn2019.employee.model.Employee;
import pl.agh.bdn2019.employee.repository.EmployeeRepository;
import pl.agh.bdn2019.employee.dto.CreateOrUpdateEmployeeDto;
import pl.agh.bdn2019.employee.dto.EmployeeDto;
import pl.agh.bdn2019.employee.model.EmployeeBuilder;


import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EmployeeService implements CrudService<Integer, EmployeeDto, CreateOrUpdateEmployeeDto> {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeAssembler employeeAssembler;

    @Override
    public Integer create(CreateOrUpdateEmployeeDto dto) {
        Employee employee = EmployeeBuilder.builder(employeeRepository)
                .name(dto.getTitleOfCourtesy(),dto.getFirstName(),dto.getLastName())
                .title(dto.getTitle())
                .reportsTo(dto.getReportsTo())
                .address(dto.getAddress())
                .born(dto.getBirthDate())
                .hired(dto.getHireDate())
                .phone(dto.getHomePhone())
                .photo(dto.getPhoto())
                .notes(dto.getNotes())
                .build();

        return employeeRepository.save(employee).getId();
    }

    @Override
    public void update(Integer integer, CreateOrUpdateEmployeeDto dto) {
        Employee employee = employeeRepository.getById(integer);

        EmployeeBuilder.builder(employeeRepository)
                .existing(employee)
                .name(dto.getTitleOfCourtesy(),dto.getFirstName(),dto.getLastName())
                .title(dto.getTitle())
                .reportsTo(dto.getReportsTo())
                .address(dto.getAddress())
                .born(dto.getBirthDate())
                .hired(dto.getHireDate())
                .phone(dto.getHomePhone())
                .photo(dto.getPhoto())
                .notes(dto.getNotes())
                .build();
    }

    @Override
    public void delete(Integer integer) {
        employeeRepository.delete(employeeRepository.getById(integer));
    }

    @Override
    public EmployeeDto get(Integer integer) {
        return employeeAssembler.toDto(employeeRepository.getById(integer));
    }

    @Override
    public List<EmployeeDto> getAll() {
        return employeeAssembler.toDtoList(employeeRepository.findAll());
    }
}
