package pl.agh.bdn2019.common.security;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * Utility service for managing redis
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class RedisService {

    private RedisTemplate<String, UserDetails> template;

    public void add(String key, UserDetails object) {
        template.opsForValue().set(key, object);
    }

    public UserDetails get(String key) {
        return template.opsForValue().get(key);
    }

    public boolean exists(String key) {
        Boolean result = template.hasKey(key);
        return result == null ? false : result;
    }

    public void delete(String key) {
        template.delete(key);
    }
}
