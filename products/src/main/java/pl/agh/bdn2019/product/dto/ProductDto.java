package pl.agh.bdn2019.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Data transfer object for product category.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Product category object")
public class ProductDto implements Serializable {

	@ApiModelProperty(notes = "Entity id, cannot be negative")
	private Integer id;
	
	@ApiModelProperty(notes = "Product name (Required)")
	@NotEmpty
	private String name;
	
	@ApiModelProperty(notes = "Supplier")
	private SupplierDto supplier;
	
	@ApiModelProperty(notes = "Category")
	private CategoryDto category;
	
	@ApiModelProperty(notes = "Quantity per unit")
	private String quantityPerUnit;
	
	@ApiModelProperty(notes = "Unit Price")
	private BigDecimal unitPrice;
	
	@ApiModelProperty(notes = "Units in stock")
	private Integer unitsInStock;
	
	@ApiModelProperty(notes = "Units in order")
	private Integer unitsOnOrder;
	
	@ApiModelProperty(notes = "Reorder level")
	private Integer reorderLevel;
	
	@ApiModelProperty(notes = "Is discontinued")
	private boolean discontinued;
	
}
