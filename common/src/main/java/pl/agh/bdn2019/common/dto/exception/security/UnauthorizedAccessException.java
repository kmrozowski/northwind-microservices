package pl.agh.bdn2019.common.dto.exception.security;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

public class UnauthorizedAccessException extends ApplicationException {

    public UnauthorizedAccessException() {
        super("UNAUTHORIZED_ACCESS", new Object[]{});
    }
}
