package pl.agh.bdn2019.employee.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.agh.bdn2019.common.dto.AddressDto;

import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Create or update employee")
public class CreateOrUpdateEmployeeDto {
    private String firstName;
    private String lastName;
    private String title;
    private String titleOfCourtesy;
    private LocalDate birthDate;
    private LocalDate hireDate;
    private AddressDto address;
    private String homePhone;
    private String notes;
    private Integer reportsTo;
    private String photo;
    Set<Integer> territories;
}
