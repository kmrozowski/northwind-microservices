package pl.agh.bdn2019.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.agh.bdn2019.user.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    Boolean existsByEmail(String email);

    Boolean existsByUsername(String username);
}
