package pl.agh.bdn2019.common.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO class represents business response error
 */
@ApiModel(description = "Error object")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public
class ErrorDto implements Serializable {
	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = -6541205610345895989L;

	/**
	 * Human readable error code
	 */
	@ApiModelProperty(notes = "Error code")
	private String errorCode;

	/**
	 * Translated error message.
	 */
	@ApiModelProperty(notes = "Translated error message, based on locale header.")
	private String translation;

}
