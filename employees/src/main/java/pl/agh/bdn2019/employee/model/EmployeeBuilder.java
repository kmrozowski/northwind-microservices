package pl.agh.bdn2019.employee.model;

import pl.agh.bdn2019.common.dto.AddressDto;
import pl.agh.bdn2019.common.types.Address;
import pl.agh.bdn2019.employee.repository.EmployeeRepository;

import java.time.LocalDate;
import java.util.Set;

public class EmployeeBuilder {
    private Employee existing;

    private String firstName;
    private String lastName;
    private String title;
    private String titleOfCourtesy;
    private LocalDate birthDate;
    private LocalDate hireDate;
    private AddressDto address;
    private String homePhone;
    private String notes;
    private Employee reportsTo;
    private String photoPath;

    private Set<Territory> territories;

    private EmployeeRepository employeeRepository;

    public static EmployeeBuilder builder(EmployeeRepository employeeRepository) {
        return new EmployeeBuilder(employeeRepository);
    }

    public EmployeeBuilder existing(Employee employee) {
        this.existing = employee;
        return this;
    }

    public EmployeeBuilder name(String titleOfCourtesy, String firstName, String lastName) {
        this.titleOfCourtesy = titleOfCourtesy;
        this.firstName = firstName;
        this.lastName = lastName;
        return this;
    }

    public EmployeeBuilder title(String title) {
        this.title = title;
        return this;
    }

    public EmployeeBuilder phone(String phone) {
        this.homePhone = phone;
        return this;
    }

    public EmployeeBuilder reportsTo(Integer id) {
        if(id != null) {
            reportsTo = employeeRepository.getById(id);
        }
        return this;
    }

    public EmployeeBuilder hired(LocalDate hireDate) {
        this.hireDate = hireDate;
        return this;
    }

    public EmployeeBuilder born(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public EmployeeBuilder photo(String photo) {
        this.photoPath = photo;
        return this;
    }

    public EmployeeBuilder notes(String notes) {
        this.notes = notes;
        return this;
    }

    public EmployeeBuilder address(AddressDto address) {
        this.address = address;
        return this;
    }

    public Employee build() {
        if(existing != null) {
            setValues(existing);
            return existing;
        } else {
            Employee employee = new Employee();
            setValues(employee);
            return employee;
        }
    }

    private void setValues(Employee employee) {
        employee.setTitleOfCourtesy(titleOfCourtesy);
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setTitle(title);
        employee.setHomePhone(homePhone);
        employee.setReportsTo(reportsTo);
        employee.setHireDate(hireDate);
        employee.setBirthDate(birthDate);
        employee.setPhotoPath(photoPath);
        employee.setNotes(notes);
        employee.setAddress(Address.builder()
                .address(address.getAddress())
                .city(address.getCity())
                .country(address.getCountry())
                .postalCode(address.getPostalCode())
                .region(address.getRegion())
                .build());
    }

    private EmployeeBuilder(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }
}
