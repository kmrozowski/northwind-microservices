package pl.agh.bdn2019.customer.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.agh.bdn2019.common.dto.AddressDto;
import pl.agh.bdn2019.common.model.EntityWithID;
import pl.agh.bdn2019.common.types.Address;

import javax.persistence.*;
import java.util.Collections;
import java.util.Set;

/**
 * Customer entity
 */
@Entity
@Table(name = "Customers")
@AttributeOverride(name = "id", column = @Column(name="CustomerID"))
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Customer extends EntityWithID<Integer> {

    @Column(name = "CompanyName")
    private String companyName;

    @Column(name = "ContactName")
    private String contactName;

    @Column(name = "ContactTitle")
    private String contactTitle;

    @Embedded
    private Address address;

    @OneToMany
    @JoinTable(
            name = "CustomerCustomerDemo",
            joinColumns = @JoinColumn(name = "CustomerID"),
            inverseJoinColumns = @JoinColumn(name = "CustomerTypeID")
    )
    private Set<CustomerDemographic> demographics = Collections.emptySet();

    public static Customer create(String companyName, String contactName, String contactTitle, AddressDto address) {
        Customer customer = new Customer();
        customer.companyName = companyName;
        customer.contactName = contactName;
        customer.contactTitle = contactTitle;
        customer.address = Address.builder()
                .address(address.getAddress())
                .city(address.getCity())
                .country(address.getCountry())
                .postalCode(address.getPostalCode())
                .region(address.getRegion())
                .build();

        return customer;
    }

    public void update(String companyName, String contactName, String contactTitle, AddressDto address) {
        this.companyName = companyName;
        this.contactName = contactName;
        this.contactTitle = contactTitle;
        this.address = Address.builder()
                .address(address.getAddress())
                .city(address.getCity())
                .country(address.getCountry())
                .postalCode(address.getPostalCode())
                .region(address.getRegion())
                .build();
    }

    public void assignDemographics(Set<CustomerDemographic> demographics) {
        this.demographics = demographics;
    }

    public void addDemographic(CustomerDemographic demographic) {
        this.demographics.add(demographic);
    }

    public void addDemographics(Set<CustomerDemographic> demographics) {
        this.demographics.addAll(demographics);
    }
}
