package pl.agh.bdn2019.user.client;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsClientService {
    @Autowired
    private UserDetailsClient client;

    @HystrixCommand
    public Optional<UserDetails> getUser(Integer id) {
        return Optional.ofNullable(client.getUser(id).orElse(null));
    }
}
