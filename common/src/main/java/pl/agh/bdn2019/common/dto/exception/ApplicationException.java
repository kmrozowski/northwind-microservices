package pl.agh.bdn2019.common.dto.exception;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Generic class representing expected application-related exceptions.
 */
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class ApplicationException extends RuntimeException {

	/**
	 * Error code.
	 */
	private String code;

	/**
	 * Optional arguments. Used for interpolation of error message.
	 */
	private Object[] args;
	
}
