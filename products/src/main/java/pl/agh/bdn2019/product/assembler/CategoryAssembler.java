package pl.agh.bdn2019.product.assembler;

import org.springframework.stereotype.Component;

import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.product.dto.CategoryDto;
import pl.agh.bdn2019.product.model.Category;

/*
 * Assembler for product catogory
 */
@Component
public class CategoryAssembler implements AbstractAssembler<Category, CategoryDto> {

	@Override
	public CategoryDto toDto(Category entity) {
		CategoryDto dto = new CategoryDto();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setDescription(entity.getDescription());
		dto.setImage(entity.getImage() != null ? entity.getImage().getContent() : null);
		return dto;
	}

}
