package pl.agh.bdn2019.product.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import pl.agh.bdn2019.product.assembler.SupplierAssembler;
import pl.agh.bdn2019.product.dto.CreateOrUpdateSupplierRequestDto;
import pl.agh.bdn2019.product.dto.SupplierDto;
import pl.agh.bdn2019.product.model.Supplier;
import pl.agh.bdn2019.product.repository.SupplierRepository;
import pl.agh.bdn2019.common.service.CrudService;

/**
 * Supplier service
 */
@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SupplierService implements CrudService<Integer, SupplierDto, CreateOrUpdateSupplierRequestDto>{

	private SupplierRepository repository;
	
	private SupplierAssembler assembler;

	@Override
	public Integer create(CreateOrUpdateSupplierRequestDto dto) {
		Supplier supplier = Supplier.create(dto);	
		return repository.save(supplier).getId();
	}

	@Override
	public void update(Integer id, CreateOrUpdateSupplierRequestDto dto) {
		Supplier supplier = repository.getById(id);
		supplier.update(dto);
	}

	@Override
	public void delete(Integer id) {
		Supplier supplier = repository.getById(id);
		repository.delete(supplier);
	}

	@Override
	public SupplierDto get(Integer id) {
		return assembler.toDto(repository.getById(id));
	}

	@Override
	public List<SupplierDto> getAll() {
		return assembler.toDtoList(repository.findAll());
	}
	
}
