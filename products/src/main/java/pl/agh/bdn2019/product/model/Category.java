package pl.agh.bdn2019.product.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.agh.bdn2019.common.model.EntityWithID;
import pl.agh.bdn2019.common.types.Image;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * Category entity
 */
@Entity
@Table(name = "Categories")
@AttributeOverrides({
	@AttributeOverride(name = "id", column = @Column(name="CategoryID"))
})
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Category extends EntityWithID<Integer> {
	
	@Column(name="CategoryName")
	@NotEmpty
	private String name;
	
	@Column(name="Description")
	private String description;
	
	@AttributeOverride(name = "content", column = @Column(name = "Picture"))
	private Image image;

	
	public static Category create(String name, String description) {
		return new Category(name, description, null);
	}
	
	public static Category create(String name, String description, byte[] image) {
		return new Category(name, description, Image.create(image));
	}
	
	public void update(String name, String description, byte[] image) {
		this.name = name;
		this.description = description;
		this.image = Image.create(image);
	}
	
}
