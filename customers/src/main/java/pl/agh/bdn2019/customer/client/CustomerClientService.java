package pl.agh.bdn2019.customer.client;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.customer.dto.CustomerDto;

@Service
public class CustomerClientService {

    @Autowired
    private CustomerClient client;

    @HystrixCommand
    public CustomerDto get(Integer id) {
        return client.get(id).orElseThrow();
    }
}
