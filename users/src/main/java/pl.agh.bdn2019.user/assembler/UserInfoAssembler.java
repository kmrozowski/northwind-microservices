package pl.agh.bdn2019.user.assembler;

import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.user.dto.UserInfoDto;
import pl.agh.bdn2019.user.model.Role;
import pl.agh.bdn2019.user.model.User;

import java.util.stream.Collectors;

@Component
public class UserInfoAssembler implements AbstractAssembler<User, UserInfoDto> {

    @Override
    public UserInfoDto toDto(User entity) {
        UserInfoDto dto = new UserInfoDto();
        dto.setActive(entity.getActive());
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setIsEmailVerified(entity.getIsEmailVerified());
        dto.setLastName(entity.getLastName());
        dto.setRoles(entity.getRoles().stream()
                .map(Role::getRole)
                .map(Enum::name)
                .collect(Collectors.toSet())
        );
        dto.setUsername(entity.getUsername());
        return dto;
    }
}
