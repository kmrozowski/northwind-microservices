package pl.agh.bdn2019.employee.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.common.assembler.AddressAssembler;
import pl.agh.bdn2019.common.model.EntityWithID;
import pl.agh.bdn2019.employee.dto.EmployeeDto;
import pl.agh.bdn2019.employee.model.Employee;

@Component
public class EmployeeAssembler implements AbstractAssembler<Employee, EmployeeDto> {
    @Autowired
    private TerritoryAssembler territoryAssembler;

    @Autowired
    private AddressAssembler addressAssembler;

    @Override
    public EmployeeDto toDto(Employee entity) {
        return EmployeeDto.builder()
                .id(entity.getId())
                .address(addressAssembler.toDto(entity.getAddress()))
                .birthDate(entity.getBirthDate())
                .firstName(entity.getFirstName())
                .hireDate(entity.getHireDate())
                .homePhone(entity.getHomePhone())
                .lastName(entity.getLastName())
                .notes(entity.getNotes())
                .photoPath(entity.getPhotoPath())
                .reportsTo(entity.getHigherUp().map(EntityWithID::getId).orElse(null))
                .title(entity.getTitle())
                .titleOfCourtesy(entity.getTitleOfCourtesy())
                .territories(territoryAssembler.toDtoSet(entity.getTerritories()))
                .build();
    }
}
