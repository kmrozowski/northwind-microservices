package pl.agh.bdn2019.order.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.agh.bdn2019.common.model.EntityWithID;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

/**
 * Shipper entity
 */
@Entity
@Table(name = "Shippers")
@AttributeOverride(name = "id", column = @Column(name="ShipperID"))
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Shipper extends EntityWithID<Integer> {

    @Column(name = "CompanyName")
    @NotEmpty
    private String companyName;

    @Column(name = "Phone")
    @NotEmpty
    private String phone;

    public static Shipper create(String companyName, String phone) {
        return new Shipper(companyName, phone);
    }

    public void update(String companyName, String phone) {
        this.companyName = companyName;
        this.phone = phone;
    }
}
