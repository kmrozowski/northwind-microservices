package pl.agh.bdn2019.product.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import pl.agh.bdn2019.product.assembler.CategoryAssembler;
import pl.agh.bdn2019.product.dto.CategoryDto;
import pl.agh.bdn2019.product.dto.CreateOrUpdateCategoryDto;
import pl.agh.bdn2019.product.model.Category;
import pl.agh.bdn2019.product.repository.CategoryRepository;
import pl.agh.bdn2019.common.service.CrudService;

/**
 * Category service
 */
@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryService implements CrudService<Integer, CategoryDto, CreateOrUpdateCategoryDto>{
	
	private CategoryRepository repository;
	
	private CategoryAssembler assembler;
	
	@Override
	public Integer create(CreateOrUpdateCategoryDto dto) {
		Category category = Category.create(dto.getName(), dto.getDescription(), dto.getImage());
		return repository.save(category).getId();
	}
	
	@Override
	public void update(Integer id, CreateOrUpdateCategoryDto dto) {
		Category category = repository.getById(id);
		category.update(dto.getName(), dto.getDescription(), dto.getImage());
	}
	
	@Override
	public void delete(Integer id) {
		Category category = repository.getById(id);
		repository.delete(category);
	}
	
	@Override
	public CategoryDto get(Integer id)  {
		return assembler.toDto(repository.getById(id));
	}
	
	@Override
	public List<CategoryDto> getAll() {
		return assembler.toDtoList(repository.findAll());
	}
	
}
