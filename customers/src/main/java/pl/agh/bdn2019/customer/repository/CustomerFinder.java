package pl.agh.bdn2019.customer.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.Repository;
import pl.agh.bdn2019.customer.model.Customer;

public interface CustomerFinder extends Repository<Customer, Integer>, QuerydslPredicateExecutor<Customer> {
}
