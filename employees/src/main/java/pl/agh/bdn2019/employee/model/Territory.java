package pl.agh.bdn2019.employee.model;

import pl.agh.bdn2019.common.model.EntityWithID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Employee territory entity
 */
@Entity
@Table(name = "Territories")
@AttributeOverride(name = "id", column = @Column(name="TerritoryID"))
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Territory extends EntityWithID<Integer> {

    @ManyToOne
    @JoinColumn(name = "RegionID")
    private Region region;

    @Column(name = "TerritoryDescription")
    private String description;


    public static Territory create(Region region, String description) {
        return new Territory(region, description);
    }

    public void update(Region region, String description) {
        this.region = region;
        this.description = description;
    }

}
