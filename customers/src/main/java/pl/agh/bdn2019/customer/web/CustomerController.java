package pl.agh.bdn2019.customer.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.config.PreAuthorizeTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.customer.dto.CreateOrUpdateCustomerDto;
import pl.agh.bdn2019.customer.dto.CustomerDto;
import pl.agh.bdn2019.customer.service.CustomerService;

import java.util.List;

/**
 * Web controller for customers.
 */
@RestController
@CrossOrigin
@RequestMapping()
@Api(value = "Endpoint for managing customers", tags = ApiDocumentationTags.CUSTOMER)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CustomerController {

    private CustomerService service;

    @GetMapping("/{id}")
    @ApiOperation(value = "Gets customer ith specified id", tags = {ApiDocumentationTags.CUSTOMER})
    public ResponseDto<CustomerDto> get(@ApiParam(value = "Id of the customer", example = "123") @PathVariable Integer id) {
        return ResponseDto.success(service.get(id));
    }

    @GetMapping()
    @Secured(PreAuthorizeTags.SEE_ALL_CUSTOMERS)
    @ApiOperation(value ="Get all customers", tags = {ApiDocumentationTags.CUSTOMER})
    public ResponseDto<List<CustomerDto>> getAll() {
        return ResponseDto.success(service.getAll());
    }

    @PostMapping()
    @Secured(PreAuthorizeTags.CREATE_UPDATE_CUSTOMERS)
    @ApiOperation(value ="Create customer", tags = {ApiDocumentationTags.CUSTOMER})
    public ResponseDto<Integer> create(@ApiParam("Data transfer object")
                                       @RequestBody CreateOrUpdateCustomerDto dto) {
        return ResponseDto.success(service.create(dto));
    }

    @PutMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_CUSTOMERS)
    @ApiOperation(value = "Updates customer with specified id", tags = {ApiDocumentationTags.CUSTOMER})
    public ResponseDto<Void> update(@ApiParam(value = "Id of the customer to be updated", example = "123") @PathVariable Integer id,
                                    @ApiParam("Data transfer object")
                                    @RequestBody CreateOrUpdateCustomerDto dto) {
        service.update(id, dto);
        return ResponseDto.success();
    }

    @DeleteMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_CUSTOMERS)
    @ApiOperation(value = "Deletes customer with specified id", tags = {ApiDocumentationTags.CUSTOMER})
    public ResponseDto<Void> delete(@ApiParam(value = "Id of the customer to be deleted", example = "123") @PathVariable Integer id) {
        service.delete(id);
        return ResponseDto.success();
    }



}
