package pl.agh.bdn2019.order.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.config.PreAuthorizeTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.order.dto.CreateOrUpdateOrderDto;
import pl.agh.bdn2019.order.dto.CreateOrUpdateShipperDto;
import pl.agh.bdn2019.order.dto.OrderDto;
import pl.agh.bdn2019.order.dto.ShipperDto;
import pl.agh.bdn2019.order.service.OrderService;
import pl.agh.bdn2019.order.service.ShipperService;

import java.util.List;

/**
 * Web controller for order entities and related entities.
 */
@RestController
@CrossOrigin
@RequestMapping()
@Api(value = "Endpoint for managing orders", tags = ApiDocumentationTags.ORDERS)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OrderController {

    private OrderService orderService;

    private ShipperService shipperService;

    @GetMapping("/{id}")
    //TODO prequthorize
    @ApiOperation(value ="Gets order with specified id", tags = {ApiDocumentationTags.ORDERS})
    public ResponseDto<OrderDto> get(@ApiParam("Id of the order") @PathVariable Integer id) {
        return ResponseDto.success(orderService.get(id));
    }

    @GetMapping()
    @Secured(PreAuthorizeTags.SEE_ALL_ORDERS)
    @ApiOperation(value ="Get all orders", tags = {ApiDocumentationTags.ORDERS})
    public ResponseDto<List<OrderDto>> getAll() {
        return ResponseDto.success(orderService.getAll());
    }

    @PostMapping()
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Create order", tags = {ApiDocumentationTags.ORDERS})
    public ResponseDto<Integer> create(@ApiParam("Data transfer object")
                                       @RequestBody CreateOrUpdateOrderDto dto) {
        return ResponseDto.success(orderService.create(dto));
    }

    @PutMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Updates order with specified id", tags = {ApiDocumentationTags.ORDERS})
    public ResponseDto<Void> update(@ApiParam("Id of the order to be updated") @PathVariable Integer id,
                                    @ApiParam("Data transfer object")
                                    @RequestBody CreateOrUpdateOrderDto dto) {
        orderService.update(id, dto);
        return ResponseDto.success();
    }

    @DeleteMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value ="Deletes order with specified id", tags = {ApiDocumentationTags.ORDERS})
    public ResponseDto<Void> delete(@ApiParam("Id of the order to be deleted") @PathVariable Integer id) {
        orderService.delete(id);
        return ResponseDto.success();
    }


    @GetMapping("/shipper/{id}")
    @ApiOperation(value ="Gets Shipper with specified id", tags = {ApiDocumentationTags.ORDERS, ApiDocumentationTags.SHIPPERS})
    public ResponseDto<ShipperDto> getShipper(@ApiParam("Id of the shipper") @PathVariable Integer id) {
        return ResponseDto.success(shipperService.get(id));
    }

    @GetMapping("/shipper")
    @ApiOperation(value ="Get all shippers", tags = {ApiDocumentationTags.ORDERS, ApiDocumentationTags.SHIPPERS})
    public ResponseDto<List<ShipperDto>> getAllShippers() {
        return ResponseDto.success(shipperService.getAll());
    }

    @PostMapping("/shipper")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_SHIPPERS)
    @ApiOperation(value ="Create shipper",  tags = {ApiDocumentationTags.ORDERS, ApiDocumentationTags.SHIPPERS})
    public ResponseDto<Integer> createShipper(@ApiParam("Data transfer object")
                                       @RequestBody CreateOrUpdateShipperDto dto) {
        return ResponseDto.success(shipperService.create(dto));
    }

    @PutMapping("/shipper/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_SHIPPERS)
    @ApiOperation(value ="Updates shipper with specified id",  tags = {ApiDocumentationTags.ORDERS, ApiDocumentationTags.SHIPPERS})
    public ResponseDto<Void> updateShipper(@ApiParam("Id of the shipper to be updated") @PathVariable Integer id,
                                    @ApiParam("Data transfer object")
                                    @RequestBody CreateOrUpdateShipperDto dto) {
        shipperService.update(id, dto);
        return ResponseDto.success();
    }

    @DeleteMapping("/shipper/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_SHIPPERS)
    @ApiOperation(value ="Deletes shipper with specified id",  tags = {ApiDocumentationTags.ORDERS, ApiDocumentationTags.SHIPPERS})
    public ResponseDto<Void> deleteShipper(@ApiParam("Id of the shipper to be deleted") @PathVariable Integer id) {
        shipperService.delete(id);
        return ResponseDto.success();
    }

}
