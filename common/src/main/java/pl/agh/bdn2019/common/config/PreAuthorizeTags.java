package pl.agh.bdn2019.common.config;

/**
 * List of available roles for pre authorization.
 */
public class PreAuthorizeTags {
    /**
     * Create, Update and delete products and related entities.
     */
    public static final String CREATE_UPDATE_ORDERS = "PRODUCT_CREATE";
    public static final String CREATE_UPDATE_EMPLOYEES = "EMPLOYEE_CREATE";

    /**
     * Read all products.
     */
    public static final String SEE_ALL_PRODUCTS = "PRODUCT_READ_ALL";
    public static final String SEE_ALL_EMPLOYEES = "EMPLOYEE_READ_ALL";

    /**
     * Create, Update and delete shippers
     */
    public static final String CREATE_UPDATE_SHIPPERS = "SHIPPER_CREATE";
    public static final String CREATE_UPDATE_CUSTOMERS = "CUSTOMER_CREATE";
    public static final String CREATE_UPDATE_DEMOGRAPHICS = "DEMOGRAPHICS_CREATE";

    /**
     * Read all orders.
     */
    public static final String SEE_ALL_ORDERS = "ORDERS_READ_ALL";

    /**
     * Read all products.
     */
    public static final String SEE_ALL_CUSTOMERS = "CUSTOMER_READ_ALL";

    public static final String SEE_ALL_SHIPPERS = "SHIPPER_READ_ALL";
}
