package pl.agh.bdn2019.customer.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data transfer object for searching customers.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Customer search criteria")
public class SearchCustomerCriteriaDto {

    private String companyName;
    private String contactName;
    private String address;
    private String city;
    private String region;
    private String postalCode;
    private String country;

}
