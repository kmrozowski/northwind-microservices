package pl.agh.bdn2019.employee.model;

import lombok.*;
import pl.agh.bdn2019.common.model.EntityWithID;
import pl.agh.bdn2019.common.types.Address;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

/**
 * Employee entity
 */
@Entity
@Table(name = "Employees")
@AttributeOverride(name = "id", column = @Column(name="EmployeeID"))
@Getter
@Setter(value = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Employee extends EntityWithID<Integer> {

    /**
     * Employee first name.
     */
    @Column(name = "FirstName")
    @NotEmpty
    private String firstName;

    /**
     * Employee last name
     */
    @Column(name = "LastName")
    @NotEmpty
    private String lastName;

    /**
     * Job title. For example "Sales Representative"
     */
    @Column(name = "Title")
    @NotEmpty
    private String title;

    /**
     * Title of courtesy. For example "Mr.", "Dr."
     */
    @Column(name = "TitleOfCourtesy")
    @NotEmpty
    private String titleOfCourtesy;

    /**
     * Employee date of birth
     */
    @Column(name = "BirthDate")
    @Past
    private LocalDate birthDate;

    /**
     * Employee hire date
     */
    @Column(name = "HireDate")
    @PastOrPresent
    private LocalDate hireDate;

    /**
     * Employee address
     */
    @Embedded
    private Address address;

    /**
     * Employee private phone number
     */
    @Column(name = "HomePhone")
    private String homePhone;

    private String notes;

    @OneToOne
    @JoinColumn(name = "EmployeeID")
    private Employee reportsTo;

    @Column(name = "Photo")
    private String photoPath;

    @OneToMany
    @JoinTable(name = "EmployeeTerritories",
            joinColumns = @JoinColumn(name = "EmployeeID"),
            inverseJoinColumns = @JoinColumn(name = "TerritoryID")
    )
    private Set<Territory> territories;

    public Optional<Employee> getHigherUp() {
        return Optional.ofNullable(reportsTo);
    }

    public void assignHigherUp(Employee employee) {
        this.reportsTo = employee;
    }

}
