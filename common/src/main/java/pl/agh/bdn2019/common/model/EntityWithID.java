package pl.agh.bdn2019.common.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


/**
 * Class representing entity with numeric ID
 */
@MappedSuperclass
@Getter
@Setter(AccessLevel.PROTECTED)
public abstract class EntityWithID<T> {
	/**
	 * Database ID. Uses generation strategy provided by database.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", updatable = false, nullable = false)
	private T id;

	/**
	 * Checks if record is persisted in the database
	 * @return
	 */
	public boolean isNew() {
		return id != null;
	}

}
