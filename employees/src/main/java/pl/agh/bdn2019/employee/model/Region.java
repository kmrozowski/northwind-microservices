package pl.agh.bdn2019.employee.model;

import pl.agh.bdn2019.common.model.EntityWithID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Employee region entity
 */
@Entity
@Table(name = "Region")
@AttributeOverride(name = "id", column = @Column(name="RegionID"))
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Region extends EntityWithID<Integer> {

    @Column(name = "RegionDescription")
    private String description;

    public static Region create(String description) {
        return new Region(description);
    }

    public void update(String description) {
        this.description = description;
    }
}
