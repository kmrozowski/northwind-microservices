package pl.agh.bdn2019.order.dto.exception;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

public class ShipperNotFoundException extends ApplicationException {

    public ShipperNotFoundException(Integer id) {
        super("SHIPPER_NOT_FOUND", new Object[]{id});
    }
}