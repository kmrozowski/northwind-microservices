package pl.agh.bdn2019.customer.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.common.service.CrudService;
import pl.agh.bdn2019.customer.assembler.CustomerAssembler;
import pl.agh.bdn2019.customer.dto.CreateOrUpdateCustomerDto;
import pl.agh.bdn2019.customer.dto.CustomerDto;
import pl.agh.bdn2019.customer.model.Customer;
import pl.agh.bdn2019.customer.model.CustomerDemographic;
import pl.agh.bdn2019.customer.repository.CustomerDemographicRepository;
import pl.agh.bdn2019.customer.repository.CustomerRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Customer service
 */
@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CustomerService implements CrudService<Integer, CustomerDto, CreateOrUpdateCustomerDto> {

    private CustomerRepository repository;

    private CustomerAssembler assembler;

    private CustomerDemographicRepository demographicRepository;

    @Override
    public Integer create(CreateOrUpdateCustomerDto dto) {

        Customer customer = Customer.create(
                dto.getCompanyName(),
                dto.getContactName(),
                dto.getContactTitle(),
                dto.getAddress()
        );

        return repository.save(customer).getId();
    }

    @Override
    public void update(Integer integer, CreateOrUpdateCustomerDto dto) {
        Customer customer = repository.getById(integer);
        customer.update(dto.getCompanyName(),
                dto.getContactName(),
                dto.getContactTitle(),
                dto.getAddress()
        );
    }

    @Override
    public void delete(Integer integer) {
        repository.delete(repository.getById(integer));
    }

    @Override
    public CustomerDto get(Integer integer) {
        return assembler.toDto(repository.getById(integer));
    }

    @Override
    public List<CustomerDto> getAll() {
        return assembler.toDtoList(repository.findAll());
    }

    /**
     * Creates demographic category
     * @param description Brief description
     * @return Id of persisted entity
     */
    public Integer createDemograpic(String description) {
        CustomerDemographic demographic = CustomerDemographic.create(description);
        return demographicRepository.save(demographic).getId();
    }

    /**
     * Updates demographic category
     * @param id Entity id
     * @param description Brief description
     */
    public void updateDemographic(Integer id, String description) {
        demographicRepository.getById(id).update(description);
    }

    /**
     * Deletes demographic category
     * @param id Entity id
     */
    public void deleteDemographic(Integer id) {
        demographicRepository.deleteById(id);
    }
}
