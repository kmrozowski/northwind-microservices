package pl.agh.bdn2019.order.assembler;

import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.order.dto.OrderDetailsDto;
import pl.agh.bdn2019.order.model.OrderDetails;

/**
 * Assembler for order positions.
 */
@Component
public class OrderDetailsAssembler implements AbstractAssembler<OrderDetails, OrderDetailsDto> {
    @Override
    public OrderDetailsDto toDto(OrderDetails entity) {
         OrderDetailsDto dto = new OrderDetailsDto();
         dto.setDiscount(entity.getDiscount());
         dto.setQuantity(entity.getQuantity());
         dto.setUnitPrice(entity.getUnitPrice());
         dto.setProductId(entity.getProductId());
         return dto;
    }
}
