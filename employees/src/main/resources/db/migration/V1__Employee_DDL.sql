CREATE SCHEMA Northwind;

CREATE TABLE IF NOT EXISTS Northwind.Employees
(
    EmployeeID      SERIAL,
    LastName        varchar(20) default NULL,
    FirstName       varchar(20) default NULL,
    Title           varchar(30) default NULL,
    TitleOfCourtesy varchar(25) default NULL,
    BirthDate       date        default NULL,
    HireDate        date        default NULL,
    Address         varchar(60) default NULL,
    City            varchar(15) default NULL,
    Region          varchar(15) default NULL,
    PostalCode      varchar(10) default NULL,
    Country         varchar(15) default NULL,
    HomePhone       varchar(24) default NULL,
    Extension varchar(4) default NULL,
    Photo varchar(40) default NULL,
    Notes text,
    ReportsTo int default NULL,
    PRIMARY KEY  (EmployeeID)
);