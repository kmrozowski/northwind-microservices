package pl.agh.bdn2019.customer.dto.exception;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

/**
 * Exception thrown when customer demographic is not found.
 */
public class DemographicNotFoundException extends ApplicationException  {

    public DemographicNotFoundException(Integer id) {
        super("CUSTOMER_DEMOGRAPHIC_NOT_FOUND", new Object[]{id});
    }
}
