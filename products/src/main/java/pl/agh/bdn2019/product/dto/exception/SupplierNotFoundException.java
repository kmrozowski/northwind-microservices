package pl.agh.bdn2019.product.dto.exception;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

/**
 *  Exception thrown when supplier with specified id is not found.
 */
public class SupplierNotFoundException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5486835879559025025L;

	public SupplierNotFoundException(Integer id) {
		super("SUPPLIER_NOT_FOUND", new Object[]{id});
	}

}
