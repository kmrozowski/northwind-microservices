package pl.agh.bdn2019.common.types;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Value type for image
 */
@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Image {

	/**
	 * Image contents.
	 */
	@Column(name="Image")
	private byte[] content;

	/**
	 * Creates instance of the image
	 * @param content bytes
	 * @return new instance
	 */
	public static Image create(byte[] content) {
		Image image = new Image();
		image.content = content;
		return image;
	}
}
