package pl.agh.bdn2019.employee.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.config.PreAuthorizeTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.employee.service.EmployeeService;
import pl.agh.bdn2019.employee.dto.CreateOrUpdateEmployeeDto;
import pl.agh.bdn2019.employee.dto.EmployeeDto;

import java.util.List;

/**
 * Web controller for employees.
 */
@RestController
@CrossOrigin
@RequestMapping()
@Api(value = "Endpoint for managing employees", tags = ApiDocumentationTags.EMPLOYEE)
public class EmployeeController {
    @Autowired
    private EmployeeService service;
    
    @GetMapping("/{id}")
    @ApiOperation(value ="Gets employee with specified id", tags = {ApiDocumentationTags.EMPLOYEE})
    public ResponseDto<EmployeeDto> get(@ApiParam("Id of the employee") @PathVariable Integer id) {
        return ResponseDto.success(service.get(id));
    }

    @GetMapping()
    @Secured(PreAuthorizeTags.SEE_ALL_EMPLOYEES)
    @ApiOperation(value ="Get all employees", tags = {ApiDocumentationTags.EMPLOYEE})
    public ResponseDto<List<EmployeeDto>> getAll() {
        return ResponseDto.success(service.getAll());
    }

    @PostMapping()
    @Secured(PreAuthorizeTags.CREATE_UPDATE_EMPLOYEES)
    @ApiOperation(value ="Create employee", tags = {ApiDocumentationTags.EMPLOYEE})
    public ResponseDto<Integer> create(@ApiParam("Data transfer object")
                                       @RequestBody CreateOrUpdateEmployeeDto dto) {
        return ResponseDto.success(service.create(dto));
    }

    @PutMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_EMPLOYEES)
    @ApiOperation(value ="Updates employee with specified id", tags = {ApiDocumentationTags.EMPLOYEE})
    public ResponseDto<Void> update(@ApiParam("Id of the employee to be updated") @PathVariable Integer id,
                                    @ApiParam("Data transfer object")
                                    @RequestBody CreateOrUpdateEmployeeDto dto) {
        service.update(id, dto);
        return ResponseDto.success();
    }

    @DeleteMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_EMPLOYEES)
    @ApiOperation(value ="Deletes employee with specified id", tags = {ApiDocumentationTags.EMPLOYEE})
    public ResponseDto<Void> delete(@ApiParam("Id of the employee to be deleted") @PathVariable Integer id) {
        service.delete(id);
        return ResponseDto.success();
    }
}
