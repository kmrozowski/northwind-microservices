package pl.agh.bdn2019.product.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import pl.agh.bdn2019.product.dto.exception.CategoryNotFoundException;
import pl.agh.bdn2019.product.model.Category;

/**
 * Category repository.
 */
public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer>{

	/**
	 * Gets category by id or throw exception if it does not exists.
	 * @param id Entity id
	 * @return product category
	 */
	default Category getById(Integer id) {
		return findById(id).orElseThrow(() -> new CategoryNotFoundException(id));
	}
}
