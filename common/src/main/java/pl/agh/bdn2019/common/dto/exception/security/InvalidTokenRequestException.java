package pl.agh.bdn2019.common.dto.exception.security;


import pl.agh.bdn2019.common.dto.exception.ApplicationException;

public class InvalidTokenRequestException extends ApplicationException {

    public InvalidTokenRequestException(String token) {
        super("INVALID_TOKEN_REQUEST", new Object[]{token});
    }

    public InvalidTokenRequestException(String subCode, String token) {
        super(subCode, new Object[]{token});
    }
}
