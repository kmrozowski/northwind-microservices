package pl.agh.bdn2019.product.client;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.product.dto.CreateOrUpdateProductDto;
import pl.agh.bdn2019.product.dto.ProductDto;

import java.util.List;

@Service
public class ProductClientService {

    @Autowired
    private ProductClient client;

    @HystrixCommand(fallbackMethod = "notExists")
    public Boolean exists(Integer id) {
        return client.exists(id).orElseThrow();
    }

    @HystrixCommand
    ProductDto get(Integer id) {
        return client.get(id).orElseThrow();
    }

    @HystrixCommand
    List<ProductDto> getAll() {
        return client.getAll().orElseThrow();
    }

    @HystrixCommand
    Integer create(CreateOrUpdateProductDto dto) {
        return client.create(dto).orElseThrow();
    }

    @HystrixCommand
    void update(Integer id, CreateOrUpdateProductDto dto) {
        client.update(id, dto);
    }

    @HystrixCommand
    void delete(Integer id) {
        client.delete(id);
    }

    /**
     * @return
     */
    private Boolean notExists() {
        return false;
    }
}
