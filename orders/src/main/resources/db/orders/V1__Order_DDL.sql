CREATE SCHEMA Northwind;

CREATE TABLE IF NOT EXISTS Northwind.Shippers
(
    ShipperID   SERIAL,

    CompanyName VARCHAR(255),
    Phone       VARCHAR(255),

    PRIMARY KEY (ShipperID)
);

CREATE TABLE IF NOT EXISTS Northwind.Orders
(
    OrderID        SERIAL,

    CustomerID     INTEGER NOT NULL,
    EmployeeID     INTEGER,
    OrderDate      DATE,
    RequiredDate   DATE,
    ShippedDate    DATE,
    ShipVia        INTEGER NOT NULL,
    Freight        NUMERIC(3, 1),
    ShipName       varchar(40) default NULL,
    ShipAddress    varchar(60) default NULL,
    ShipCity       varchar(15) default NULL,
    ShipRegion     varchar(15) default NULL,
    ShipPostalCode varchar(10) default NULL,
    ShipCountry    varchar(15) default NULL,
    PRIMARY KEY (OrderID),

    --FOREIGN KEY (CustomerID) REFERENCES Northwind.Customers (CustomerID),
    -- FOREIGN KEY (EmployeeID) REFERENCES Northwind.Employees (EmployeeID),
    FOREIGN KEY (ShipVia) REFERENCES Northwind.Shippers (ShipperID)
);

CREATE TABLE IF NOT EXISTS Northwind.OrderDetails
(
    OrderID   INTEGER,
    ProductID INTEGER,
    UnitPrice NUMERIC(3, 1),
    Quantity  INTEGER,
    Discount  NUMERIC(3, 1),
    PRIMARY KEY (OrderID, ProductID),

    FOREIGN KEY (OrderID) REFERENCES Northwind.Orders (OrderID)
   -- FOREIGN KEY (ProductID) REFERENCES Northwind.Products (ProductID)
);
