package pl.agh.bdn2019.employee.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.employee.dto.EmployeeDto;

@FeignClient("employee-client")
public interface EmployeeClient {

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    ResponseDto<EmployeeDto> get(@PathVariable Integer id);
}
