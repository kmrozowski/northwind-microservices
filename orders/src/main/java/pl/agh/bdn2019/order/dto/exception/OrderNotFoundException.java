package pl.agh.bdn2019.order.dto.exception;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

public class OrderNotFoundException extends ApplicationException {

    public OrderNotFoundException(Integer id) {
        super("ORDER_NOT_FOUND", new Object[]{id});
    }
}