package pl.agh.bdn2019.customer.assembler;

import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.customer.dto.CustomerDemographicDto;
import pl.agh.bdn2019.customer.model.CustomerDemographic;

/**
 * Assembler for customer demographics
 */
@Component
public class CustomerDemographicsAssembler implements AbstractAssembler<CustomerDemographic, CustomerDemographicDto> {
    @Override
    public CustomerDemographicDto toDto(CustomerDemographic entity) {
        return new CustomerDemographicDto(entity.getId(), entity.getDescription());
    }
}
