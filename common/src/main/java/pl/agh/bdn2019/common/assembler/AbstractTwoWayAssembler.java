package pl.agh.bdn2019.common.assembler;

/**
 * Extension of AbstractAssembler interface that allows conversion from DTO to domain model.
 * @param <T> Entity class
 * @param <D> DTO class
 */
public interface AbstractTwoWayAssembler<T, D> extends AbstractAssembler<T, D> {
	/**
	 * Convert DTO to entity class
	 * @param dto Dto object
	 * @return Domain class
	 */
	T toEntity(D dto);
}
