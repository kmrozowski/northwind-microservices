package pl.agh.bdn2019.product.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.product.assembler.ProductAssembler;
import pl.agh.bdn2019.product.dto.ProductSearchCriteriaDto;
import pl.agh.bdn2019.product.dto.ProductDto;
import pl.agh.bdn2019.product.model.QProduct;
import pl.agh.bdn2019.product.repository.ProductFinder;
import pl.agh.bdn2019.common.service.AbstractFinder;

import java.util.List;
import java.util.Objects;


/**
 * Product finder service
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductFinderService extends AbstractFinder<ProductSearchCriteriaDto, ProductDto> {
    private ProductFinder finder;

    private ProductAssembler assembler;

    @Override
    public List<ProductDto> search(ProductSearchCriteriaDto criteriaDto) {
        QProduct q = QProduct.product;
        BooleanBuilder bb = new BooleanBuilder();
        addNonEmpty(q.name,bb, criteriaDto.getName());
        addNonEmpty(q.supplier.companyName, bb,criteriaDto.getSupplierName());
        addNonEmpty(q.category.name, bb, criteriaDto.getCategoryName());
        addBoolean(q.discontinued, bb, criteriaDto.isDiscontinued());
        addGreaterOrEqual(q.unitsOnOrder, bb, criteriaDto.getMinUnitsOnOrder());
        addLesserOrEqual(q.unitsOnOrder, bb, criteriaDto.getMaxUnitsOnOrder());
        addGreaterOrEqual(q.unitPrice, bb, criteriaDto.getMinUnitPrice());
        addLesserOrEqual(q.unitPrice, bb, criteriaDto.getMaxUnitPrice());
        return assembler.toDtoList(finder.findAll(Objects.requireNonNull(bb.getValue())));
    }
}
