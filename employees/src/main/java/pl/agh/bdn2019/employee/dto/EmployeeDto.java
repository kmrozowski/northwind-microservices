package pl.agh.bdn2019.employee.dto;

import io.swagger.annotations.ApiModel;
import lombok.*;
import pl.agh.bdn2019.common.dto.AddressDto;

import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ApiModel(description = "Employee details")
public class EmployeeDto {
    private Integer id;
    private String firstName;
    private String lastName;
    private String title;
    private String titleOfCourtesy;
    private LocalDate birthDate;
    private LocalDate hireDate;
    private AddressDto address;
    private String homePhone;
    private String notes;
    private Integer reportsTo;
    private String photoPath;
    private Set<TerritoryDto> territories;
}
