DELETE
FROM northwind.userauthorities;
DELETE
FROM northwind.roles;
DELETE
FROM northwind.users;

INSERT INTO northwind.roles (RoleId, RoleName)
VALUES (1, 'USER'),
(2, 'ADMIN');
ALTER SEQUENCE northwind.roles_roleid_seq RESTART WITH 3;

INSERT INTO northwind.users (UserId, Email, Username, password, FirstName, LastName, Active, EmailVerified)
VALUES (1, 'admin@admin.com', 'admin', '$2y$10$3yBgU0ynk64CvemEkC./D.1SO.4.Jo/T12OrkySmlriaofrDH8X6m', 'admin', 'admin',
        true, true);

ALTER SEQUENCE northwind.users_userid_seq RESTART WITH 2;

INSERT INTO Northwind.UserAuthorities (UserID, RoleID)
VALUES (1, 2);