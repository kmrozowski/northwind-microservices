package pl.agh.bdn2019.product.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.config.PreAuthorizeTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.product.dto.CreateOrUpdateSupplierRequestDto;
import pl.agh.bdn2019.product.dto.SupplierDto;
import pl.agh.bdn2019.product.service.SupplierService;

import java.util.List;

/**
 * Web controller for suppliers.
 */
@RestController
@CrossOrigin
@RequestMapping("/supplier")
@Api(value = "Endpoint for managing supplier", tags = ApiDocumentationTags.SUPPLIERS)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SupplierController {

    private SupplierService supplierService;

    @GetMapping("/{id}")
    @Secured("permitAll")
    @ApiOperation(value = "Gets supplier with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<SupplierDto> get(@ApiParam(value = "Id of the supplier", example = "123") @PathVariable Integer id) {
        return ResponseDto.success(supplierService.get(id));
    }

    @GetMapping
    @Secured("permitAll")
    @ApiOperation(value = "Get all suppliers",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<List<SupplierDto>> getAll() {
        return ResponseDto.success(supplierService.getAll());
    }

    @PostMapping
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value = "Create supplier",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<Integer> create(@ApiParam("Data transfer object")
                                       @RequestBody CreateOrUpdateSupplierRequestDto dto) {
        return ResponseDto.success(supplierService.create(dto));
    }

    @PutMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value = "Updates supplier with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<Void> update(@ApiParam(value = "Id of the supplier to be updated", example = "123") @PathVariable Integer id,
                                    @ApiParam("Data transfer object")
                                    @RequestBody CreateOrUpdateSupplierRequestDto dto) {
        supplierService.update(id, dto);
        return ResponseDto.success();
    }

    @DeleteMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_ORDERS)
    @ApiOperation(value = "Deletes supplier with specified id",
            tags = {ApiDocumentationTags.PRODUCT, ApiDocumentationTags.SUPPLIERS})
    public ResponseDto<Void> delete(@ApiParam(value = "Id of the supplier to be updated", example = "123") @PathVariable Integer id) {
        supplierService.delete(id);
        return ResponseDto.success();
    }
}
