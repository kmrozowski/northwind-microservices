package pl.agh.bdn2019.order.model;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.agh.bdn2019.common.dto.AddressDto;
import pl.agh.bdn2019.common.types.Address;
import pl.agh.bdn2019.order.dto.OrderDetailsDto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderBuilder {

    private Integer customer;
    private Shipper shipper;
    private BigDecimal freight;
    private Set<OrderDetailsDto> details;
    private LocalDate requiredDate;
    private AddressDto address;

    public static OrderBuilder builder() {
        return new OrderBuilder();
    }

    public OrderBuilder customer(Integer customer) {
        this.customer = customer;
        return this;
    }

    public OrderBuilder freight(BigDecimal freight) {
        this.freight = freight;
        return this;
    }

    public OrderBuilder positions(Set<OrderDetailsDto> dto) {
        this.details = dto;
        return this;
    }

    public OrderBuilder requiredDate(LocalDate date) {
        this.requiredDate = date;
        return this;
    }

    public OrderBuilder shipAddress(AddressDto address) {
        this.address = address;
        return this;
    }

    public OrderBuilder shipper(Shipper shipper) {
        this.shipper = shipper;
        return this;
    }

    public Order build() {
        Order order = new Order();
        order.setCustomerId(customer);
        order.setFreight(freight);
        order.setOrderDate(LocalDate.now());
        order.setOrderDetails(details.stream().map(d -> createPosition(order, d)).collect(Collectors.toSet()));
        order.setRequiredDate(requiredDate);
        order.setShipAddress(Address.builder()
                .address(address.getAddress())
                .city(address.getCity())
                .country(address.getCountry())
                .postalCode(address.getPostalCode())
                .region(address.getRegion())
                .build());

        order.setShipper(shipper);

        return order;
    }

    private OrderDetails createPosition(Order order, OrderDetailsDto dto) {
       return new OrderDetails(order, dto.getProductId(), dto.getUnitPrice(), dto.getQuantity(), dto.getDiscount());
    }
}
