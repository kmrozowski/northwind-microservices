package pl.agh.bdn2019.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.common.service.CrudService;
import pl.agh.bdn2019.order.assembler.ShippersAssembler;
import pl.agh.bdn2019.order.dto.CreateOrUpdateShipperDto;
import pl.agh.bdn2019.order.dto.ShipperDto;
import pl.agh.bdn2019.order.model.Shipper;
import pl.agh.bdn2019.order.repository.ShipperRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ShipperService implements CrudService<Integer, ShipperDto, CreateOrUpdateShipperDto> {

    @Autowired
    private ShipperRepository repository;

    @Autowired
    private ShippersAssembler assembler;

    @Override
    public Integer create(CreateOrUpdateShipperDto dto) {
        Shipper shipper = Shipper.create(dto.getCompanyName(),dto.getPhone());
        return repository.save(shipper).getId();
    }

    @Override
    public void update(Integer integer, CreateOrUpdateShipperDto dto) {
        Shipper shipper = repository.getById(integer);
        shipper.update(dto.getCompanyName(),dto.getPhone());
    }

    @Override
    public void delete(Integer integer) {
        repository.delete(repository.getById(integer));
    }

    @Override
    public ShipperDto get(Integer integer) {
        return assembler.toDto(repository.getById(integer));
    }

    @Override
    public List<ShipperDto> getAll() {
        return assembler.toDtoList(repository.findAll());
    }
}
