package pl.agh.bdn2019.common.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SecurityException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.common.dto.exception.security.ExpiredTokenException;
import pl.agh.bdn2019.common.dto.exception.security.InvalidTokenRequestException;
import pl.agh.bdn2019.common.dto.exception.security.MalformedTokenException;
import pl.agh.bdn2019.common.dto.exception.security.UnsupportedTokenTypeException;

import java.nio.charset.StandardCharsets;
import java.security.Key;

@Service
@Slf4j
public class TokenService {
    @Value("${northwind.security.secret}")
    private String jwtSecret;

    @Autowired
    private RedisService redisService;

    public Integer getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(getSigningKey())
                .parseClaimsJws(token)
                .getBody();

        return Integer.parseInt(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(getSigningKey()).parseClaimsJws(authToken);
            return redisService.exists(authToken);
        } catch (MalformedJwtException ex) {
            throw new MalformedTokenException(authToken);
        } catch (ExpiredJwtException ex) {
            throw new ExpiredTokenException(authToken);
        } catch (UnsupportedJwtException ex) {
            throw new UnsupportedTokenTypeException(authToken);
        } catch (SecurityException | IllegalArgumentException ex) {
            throw new InvalidTokenRequestException(authToken);
        }
    }


    public boolean tokenExists(String authToken) {
        return redisService.exists(authToken);
    }

    private Key getSigningKey() {
        byte[] keyBytes = jwtSecret.getBytes(StandardCharsets.UTF_8);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
