package pl.agh.bdn2019.common.dto;

import java.io.Serializable;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.agh.bdn2019.common.dto.exception.ClientBusinessException;

/**
 * Base response DTO. Should be used for all endpoints.
 * @param <T> Class should be serializable or Wrapper type.
 */
@ApiModel(description = "Response data transfer object")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
@Getter
public class ResponseDto<T> implements Serializable {
	
	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = -3007368974332451275L;

	/**
	 * Response. Present when server return 2XX code
	 */
	@ApiModelProperty(notes = "Success response object")
	private T response;

	/**
	 * Error. Present when server returns 4XX, 5XX code
	 */
	@ApiModelProperty(notes = "Error object")
	private ErrorDto error;

	/**
	 * Boolean switch indicating if server returned 2XX code. Simplifies frontend response processing.
	 */
	@ApiModelProperty(notes = "Is success?")
	private boolean success;

	public T orElseThrow() {
		if(!success) {
			throw new ClientBusinessException(error);
		}
		return response;
	}

	public T orElse(T other) {
		if(!success){
			return other;
		}
		return response;
	}
	/**
	 * Constructs success response message (with status 200)
	 * @param <T> Response type
	 * @param response object
	 * @return success object
	 */
	public static <T> ResponseDto<T> success(T response) {
		return new ResponseDto<>(response, null, true);
	}
	
	/**
	 * Constructs empty success response message (with status 200 and no body)
	 * @return Void success object
	 */
	public static ResponseDto<Void> success() {
		return new ResponseDto<>(null, null, true);
	}

	/**
	 * Constructs error response
	 * @param code Human readable code. For example 'CUSTOMER_NOT_FOUND'
	 * @param message Translated message. @see AuthenticationControllerAdvice
	 * @return Response object with no response and error field present.
	 */
	public static ResponseDto error(String code, String message) {
		return new ResponseDto<Void>(null, new ErrorDto(code, message), false);
	}

	/**
	 * Constructs javax.validation error response
	 * @param code Human readable code. Should be 'VALIDATION_FAILED'
	 * @param message Translated message. @see AuthenticationControllerAdvice
	 * @param errors Object containing list of fields that triggered error with messages
	 * @return Response object with no response and error field present.
	 */
	public static ResponseDto validationError(String code, String message, Map<String, String> errors) {
		return new ResponseDto<Void>(null, new ValidationErrorDto(code, message, errors), false);
	}
}
