package pl.agh.bdn2019.product.dto.exception;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

/**
 * Exception thrown when product category with specified id is not found
 */
public class CategoryNotFoundException extends ApplicationException {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = -458772792017106403L;

	public CategoryNotFoundException(Integer id) {
		super("CATEGORY_NOT_FOUND", new Object[]{id});
	}

}
