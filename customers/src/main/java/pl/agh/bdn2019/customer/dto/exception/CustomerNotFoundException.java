package pl.agh.bdn2019.customer.dto.exception;

import pl.agh.bdn2019.common.dto.exception.ApplicationException;

/**
 * Exception thrown when customer is not found.
 */
public class CustomerNotFoundException extends ApplicationException {

    public CustomerNotFoundException(Integer id) {
        super("CUSTOMER_NOT_FOUND", new Object[]{id});
    }
}
