package pl.agh.bdn2019.common.config;

/**
 * Class containing constant values for API documentation tags.
 */
public class ApiDocumentationTags {

    /**
     * Products
     */
    public static final String PRODUCT = "Products";

    /**
     * Categories
     */
    public static final String CATEGORIES = "Categories";

    /**
     * Suppliers
     */
    public static final String SUPPLIERS = "Suppliers";

    /**
     * Orders
     */
    public static final String ORDERS = "Orders";

    /**
     * Shippers
     */
    public static final String SHIPPERS = "Shippers";

    /**
     * Customers
     */
    public static final String CUSTOMER = "Customers";

    /**
     * Employyes
     */
    public static final String EMPLOYEE = "Employees";

    /**
     * Auth
     */
    public static final String AUTHORIZATION = "Authorization";

    /**
     * User Management
     */
    public static final String USERS = "User Management";
}
