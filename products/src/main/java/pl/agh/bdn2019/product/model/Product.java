package pl.agh.bdn2019.product.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.agh.bdn2019.common.model.EntityWithID;
import pl.agh.bdn2019.product.dto.CreateOrUpdateProductDto;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

/**
 * Product entity
 */
@Entity
@Table(name = "Products")
@AttributeOverride(name = "id", column = @Column(name="ProductID"))
@Getter
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Product extends EntityWithID<Integer> {
	
	@Column(name="ProductName")
	@NotEmpty
	private String name;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SupplierID")
	private Supplier supplier;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CategoryID")
	private Category category;
	
	@Column(name = "QuantityPerUnit")
	private String quantityPerUnit;
	
	@Column(name = "UnitPrice")
	private BigDecimal unitPrice;
	
	@Column(name = "UnitsInStock")
	private Integer unitsInStock;
	
	@Column(name= "UnitsOnOrder")
	private Integer unitsOnOrder;
	
	@Column(name="ReorderLevel")
	private Integer reorderLevel;
	
	@Column(name="Discontinued")
	private boolean discontinued;

	public static Product create(Category c, Supplier s, CreateOrUpdateProductDto dto) {
		Product product = new Product();
		product.name = dto.getName();
		product.category = c;
		product.supplier = s;
		product.discontinued = dto.isDiscontinued();
		product.quantityPerUnit = dto.getQuantityPerUnit();
		product.reorderLevel = dto.getReorderLevel();
		product.unitPrice = dto.getUnitPrice();
		product.unitsInStock = dto.getUnitsInStock();
		product.unitsOnOrder = dto.getUnitsOnOrder();
		return product;
	}

	public void update(Category c, Supplier s, CreateOrUpdateProductDto dto) {
		name = dto.getName();
		category = c;
		supplier = s;
		discontinued = dto.isDiscontinued();
		quantityPerUnit = dto.getQuantityPerUnit();
		reorderLevel = dto.getReorderLevel();
		unitPrice = dto.getUnitPrice();
		unitsInStock = dto.getUnitsInStock();
		unitsOnOrder = dto.getUnitsOnOrder();
	}
}
