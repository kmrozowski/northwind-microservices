package pl.agh.bdn2019.common.dto.exception.security;

public class MalformedTokenException extends InvalidTokenRequestException {

    public MalformedTokenException(String token) {
        super("MALFORMED_TOKEN", token);
    }
}
