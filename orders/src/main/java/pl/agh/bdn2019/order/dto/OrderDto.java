package pl.agh.bdn2019.order.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.agh.bdn2019.common.dto.AddressDto;
import pl.agh.bdn2019.customer.dto.CustomerDto;
import pl.agh.bdn2019.employee.dto.EmployeeDto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

/**
 * Data transfer object for order.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Order object")
public class OrderDto {

    private CustomerDto customer;

    private EmployeeDto employee;

    private Set<OrderDetailsDto> orderDetails;

    private LocalDate orderDateTime;

    private LocalDate requiredDate;

    private LocalDate shippedDate;

    private BigDecimal freight;

    private AddressDto shipAddress;

    private ShipperDto shipper;

}
