package pl.agh.bdn2019.user.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.user.dto.UserInfoDto;
import pl.agh.bdn2019.user.service.UserService;

@RestController
@RequestMapping("/api/user")
@Api(value = "User controller", tags = ApiDocumentationTags.USERS)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private UserService userService;

    @ApiOperation(value = "Get current user data", tags = ApiDocumentationTags.USERS)
    @GetMapping()
    public ResponseDto<UserInfoDto> getCurrentUser() {
        return ResponseDto.success(userService.getCurrentUser());
    }

    @GetMapping("/{id}")
    public ResponseDto<UserDetails> getUser(@PathVariable Integer id) {return ResponseDto.success(userService.loadUserById(id));}
}
