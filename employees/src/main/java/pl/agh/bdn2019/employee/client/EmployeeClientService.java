package pl.agh.bdn2019.employee.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.agh.bdn2019.employee.dto.EmployeeDto;

@Service
public class EmployeeClientService {

    @Autowired
    private EmployeeClient client;

    public EmployeeDto get(Integer id) {
        return client.get(id).orElseThrow();
    }
}
