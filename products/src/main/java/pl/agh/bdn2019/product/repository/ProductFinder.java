package pl.agh.bdn2019.product.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.Repository;
import pl.agh.bdn2019.product.model.Product;

public interface ProductFinder extends Repository<Product, Long>, QuerydslPredicateExecutor<Product> {
}
