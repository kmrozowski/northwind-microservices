package pl.agh.bdn2019.customer.assembler;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.common.assembler.AddressAssembler;
import pl.agh.bdn2019.customer.dto.CustomerDto;
import pl.agh.bdn2019.customer.model.Customer;

/**
 * Assembler for customers.
 */
@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CustomerAssembler implements AbstractAssembler<Customer, CustomerDto> {

    private AddressAssembler addressAssembler;

    private CustomerDemographicsAssembler customerDemographicsAssembler;

    @Override
    public CustomerDto toDto(Customer entity) {
        CustomerDto dto = new CustomerDto();
        dto.setAddress(addressAssembler.toDto(entity.getAddress()));
        dto.setCompanyName(entity.getCompanyName());
        dto.setContactName(entity.getContactName());
        dto.setId(entity.getId());
        dto.setContactTitle(entity.getContactTitle());
        dto.setDemographics(customerDemographicsAssembler.toDtoSet(entity.getDemographics()));
        return dto;
    }
}
