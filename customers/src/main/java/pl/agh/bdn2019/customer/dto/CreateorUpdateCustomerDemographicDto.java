package pl.agh.bdn2019.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data transfer object for creating or updating customer demographic.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel("Create or update customer demographic object")
public class CreateorUpdateCustomerDemographicDto {
    @ApiModelProperty("Demographic description")
    private String description;
}
