package pl.agh.bdn2019.product.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.agh.bdn2019.common.dto.AddressDto;

/**
 * Data transfer object for product supplier.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Supplier object")
public class SupplierDto implements Serializable {
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -1341857603265009205L;

	@ApiModelProperty(notes = "Entity id (Required)")
	@NotNull
	private Integer id;
	
	@ApiModelProperty(notes = "Company name")
	@NotEmpty
	private String companyName;
	
	@ApiModelProperty(notes = "Contact name")
	private String contactName;
	
	@ApiModelProperty(notes = "Contact title")
	private String contactTitle;
	
	@ApiModelProperty(notes = "Address")
	private AddressDto address;
	
	@ApiModelProperty(notes = "Telephone number")
	private String phone;
	
	@ApiModelProperty(notes = "Fax number")
	private String fax;
	
	@ApiModelProperty(notes = "Homepage")
	private String homepage;
}
