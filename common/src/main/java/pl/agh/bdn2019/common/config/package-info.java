/**
 * Configuration classes for Spring Boot, Swagger, etc.
 */
package pl.agh.bdn2019.common.config;