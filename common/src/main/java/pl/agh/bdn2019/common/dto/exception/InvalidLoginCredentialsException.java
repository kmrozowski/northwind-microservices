package pl.agh.bdn2019.common.dto.exception;

public class InvalidLoginCredentialsException extends ApplicationException {

    public InvalidLoginCredentialsException() {
        super("INVALID_CREDENTIALS", new Object[]{});
    }
}
