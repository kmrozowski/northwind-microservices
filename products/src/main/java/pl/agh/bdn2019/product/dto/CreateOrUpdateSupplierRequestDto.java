package pl.agh.bdn2019.product.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.agh.bdn2019.common.types.Address;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Create product supplier object")
public class CreateOrUpdateSupplierRequestDto implements Serializable {
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -9096213711404316501L;

	@ApiModelProperty(notes = "Company name")
	@NotEmpty
	private String companyName;
	
	@ApiModelProperty(notes = "Contact name")
	private String contactName;
	
	@ApiModelProperty(notes = "Contact title")
	private String contactTitle;
	
	@ApiModelProperty(notes = "Address")
	private Address address;
	
	@ApiModelProperty(notes = "Telephone number")
	private String phone;
	
	@ApiModelProperty(notes = "Fax number")
	private String fax;
	
	@ApiModelProperty(notes = "Homepage")
	private String homepage;
}
