package pl.agh.bdn2019.employee.assembler;

import org.springframework.stereotype.Component;
import pl.agh.bdn2019.common.assembler.AbstractAssembler;
import pl.agh.bdn2019.employee.dto.RegionDto;
import pl.agh.bdn2019.employee.model.Region;

@Component
public class RegionAssembler implements AbstractAssembler<Region, RegionDto> {

    @Override
    public RegionDto toDto(Region entity) {
        return new RegionDto(entity.getId(), entity.getDescription());
    }
}
