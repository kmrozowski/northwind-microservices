package pl.agh.bdn2019.common.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * DTO class represents validation error
 */
@ApiModel(description = "Error object")
@NoArgsConstructor
@Getter
@Setter
public class ValidationErrorDto extends ErrorDto {

    /**
     * Map representing fields that triggered validation error, with detailed messages.
     */
    private Map<String, String> fieldErrors;

    public ValidationErrorDto(String errorCode, String translation, Map<String, String> fieldErrors) {
        super(errorCode, translation);
        this.fieldErrors = fieldErrors;
    }
}
