package pl.agh.bdn2019.common.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Superclass for data transfer object for pageable objects.
 * @param <T> Class should be serializable or Wrapper type.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
class PageableDto<T> implements Serializable {
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -6960747629361153009L;

	/**
	 * Current page number
	 */
	@ApiModelProperty(notes = "Page number")
	@PositiveOrZero
	private int pageNumber;

	/**
	 * Number of records on the page
	 */
	@ApiModelProperty(notes = "Page size")
	@PositiveOrZero
	private int pageSize;

	/**
	 * Total number of pages
	 */
	@ApiModelProperty(notes = "Total number of pages")
	@PositiveOrZero
	private int totalPage;

	/**
	 * Fetched records
	 */
	@ApiModelProperty(notes = "Found elements")
	@NotNull
	private List<T> elements; 
}
