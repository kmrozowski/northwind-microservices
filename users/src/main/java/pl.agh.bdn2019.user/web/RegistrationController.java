package pl.agh.bdn2019.user.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.user.service.UserService;

@RestController
@RequestMapping("/api/user/register")
@Api(value = "User registration controller", tags = ApiDocumentationTags.USERS)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class RegistrationController {

    private UserService userService;

    /**
     * Endpoint for checking if email is in use
     * @param email User email in format user@server.domain
     * @return <code>true</code> if email is already in the database, <code>false</code> otherwise
     */
    @ApiOperation(value = "Checks if the given email is in use", tags = ApiDocumentationTags.USERS)
    @GetMapping("/check/email")
    public ResponseDto<Boolean> checkEmailInUse(
            @ApiParam(value = "Email address to check against")
            @RequestParam("email") String email) {
        return ResponseDto.success(userService.existsByEmail(email));
    }

    /**
     * Endpoint for checking if username is in use
     * @param login Username. Must be single word with no non-ascii characters
     * @return <code>true</code> if username is already in the database, <code>false</code> otherwise
     */
    @ApiOperation(value = "Checks if the given username is in use", tags = ApiDocumentationTags.USERS)
    @GetMapping("/check/username")
    public ResponseDto<Boolean> checkUsernameInUse(
            @ApiParam(value = "Login to check against")
            @RequestParam("login") String login) {
        return ResponseDto.success(userService.existsByUsername(login));
    }
}
