package pl.agh.bdn2019.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@ApiModel(value = "User info", description = "The login request data transfer object")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserInfoDto {

    @ApiModelProperty("Email address")
    private String email;

    @ApiModelProperty("Username")
    private String username;

    @ApiModelProperty("First name")
    private String firstName;

    @ApiModelProperty("Last name")
    private String lastName;

    @ApiModelProperty("Is active ?")
    private Boolean active;

    @ApiModelProperty("Is email verified?")
    private Boolean isEmailVerified;

    @ApiModelProperty("User roles")
    private Set<String> roles = new HashSet<>();

}
