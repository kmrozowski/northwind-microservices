package pl.agh.bdn2019.user.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.common.security.TokenService;
import pl.agh.bdn2019.user.dto.LoginRequestDto;
import pl.agh.bdn2019.user.model.JwtUserDetails;
import pl.agh.bdn2019.user.service.GenerateTokenService;

@RestController
@Secured("permitAll")
@RequestMapping("/api/auth")
@Api(value = "Authorization Rest API", tags = {ApiDocumentationTags.AUTHORIZATION})
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuthenticationController {

    private AuthenticationManager authenticationManager;

    private GenerateTokenService tokenService;

    private PasswordEncoder encoder;

    @PostMapping("/login")
    @Secured("permitAll")
    @ApiOperation(value = "Generate JWT token", tags = {ApiDocumentationTags.AUTHORIZATION})
    public ResponseDto<String> login(
            @ApiParam("Login request")
            @RequestBody LoginRequestDto loginDto) {

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());


        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        JwtUserDetails details = (JwtUserDetails) authentication.getPrincipal();
        String token = tokenService.generateToken(details.getId());

        return ResponseDto.success(token);
    }

    @GetMapping("/logout")
    @Secured("permitAll")
    @ApiOperation(value = "Invalidate JWT token", tags = {ApiDocumentationTags.AUTHORIZATION})
    public ResponseDto<Void> logout(@RequestHeader("Authorization") String authHeader) {
        tokenService.invalidateToken(authHeader);
        return ResponseDto.success();
    }
}
