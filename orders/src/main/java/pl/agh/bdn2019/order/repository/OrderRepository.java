package pl.agh.bdn2019.order.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.agh.bdn2019.order.dto.exception.OrderNotFoundException;
import pl.agh.bdn2019.order.model.Order;

public interface OrderRepository extends PagingAndSortingRepository<Order, Integer> {

    /**
     * Gets order by id or throw exception if it does not exists.
     * @param id Entity id
     * @return product category
     */
    default Order getById(Integer id) {
        return findById(id).orElseThrow(() -> new OrderNotFoundException(id));
    }

}
