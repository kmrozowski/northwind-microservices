package pl.agh.bdn2019.product.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.agh.bdn2019.common.model.EntityWithID;
import pl.agh.bdn2019.common.types.Address;
import pl.agh.bdn2019.product.dto.CreateOrUpdateSupplierRequestDto;

import javax.persistence.*;

/**
 * Supplier entity.
 */
@Entity
@Table(name = "Suppliers")
@AttributeOverride(name = "id", column = @Column(name = "SupplierID"))
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Supplier extends EntityWithID<Integer> {

	@Column(name = "CompanyName")
	private String companyName;

	@Column(name = "ContactName")
	private String contactName;

	@Column(name = "ContactTitle")
	private String contactTitle;

	@Embedded
	private Address address;

	@Column(name = "Phone")
	private String phone;

	@Column(name = "Fax")
	private String fax;

	@Column(name = "HomePage")
	private String homepage;

	public static Supplier create(CreateOrUpdateSupplierRequestDto dto) {
		Supplier entity = new Supplier();
		entity.address = Address.builder().address(dto.getAddress().getAddress()).city(dto.getAddress().getCity())
				.country(dto.getAddress().getCountry()).postalCode(dto.getAddress().getPostalCode())
				.region(dto.getAddress().getRegion()).build();
		entity.companyName = dto.getCompanyName();
		entity.contactName = dto.getContactName();
		entity.contactTitle = dto.getContactTitle();
		entity.fax = dto.getFax();
		entity.homepage = dto.getHomepage();
		entity.phone = dto.getPhone();
		return entity;
	}

	public void update(CreateOrUpdateSupplierRequestDto dto) {
		address = Address.builder().address(dto.getAddress().getAddress()).city(dto.getAddress().getCity())
				.country(dto.getAddress().getCountry()).postalCode(dto.getAddress().getPostalCode())
				.region(dto.getAddress().getRegion()).build();
		companyName = dto.getCompanyName();
		contactName = dto.getContactName();
		contactTitle = dto.getContactTitle();
		fax = dto.getFax();
		homepage = dto.getHomepage();
		phone = dto.getPhone();
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

}
