package pl.agh.bdn2019.order.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.agh.bdn2019.order.dto.exception.ShipperNotFoundException;
import pl.agh.bdn2019.order.model.Shipper;

public interface ShipperRepository extends PagingAndSortingRepository<Shipper, Integer> {
    /**
     * Gets shipper by id or throw exception if it does not exists.
     * @param id Entity id
     * @return product category
     */
    default Shipper getById(Integer id) {
        return findById(id).orElseThrow(() -> new ShipperNotFoundException(id));
    }
}
