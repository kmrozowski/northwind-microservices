package pl.agh.bdn2019.product.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Search criteria for Product Finder
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Available criteria for searching products")
public class ProductSearchCriteriaDto {
    private String name;
    private String categoryName;
    private String supplierName;
    private boolean discontinued;

    private BigDecimal minUnitPrice;
    private BigDecimal maxUnitPrice;

    private Integer minUnitsOnOrder;
    private Integer maxUnitsOnOrder;
}
