package pl.agh.bdn2019.product.repository;

import org.springframework.data.repository.CrudRepository;
import pl.agh.bdn2019.product.dto.exception.ProductNotFoundException;
import pl.agh.bdn2019.product.model.Product;

/**
 * Product repository.
 */
public interface ProductRepository extends CrudRepository<Product, Integer> {
    /**
     * Gets product by id or throw exception if it does not exists.
     * @param id Entity id
     * @return product
     */
    default Product getById(Integer id) {
        return findById(id).orElseThrow(() -> new ProductNotFoundException(id));
    }

}
