package pl.agh.bdn2019.customer.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.agh.bdn2019.common.config.ApiDocumentationTags;
import pl.agh.bdn2019.common.config.PreAuthorizeTags;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.customer.dto.CreateorUpdateCustomerDemographicDto;
import pl.agh.bdn2019.customer.service.CustomerService;

/**
 * Web controller for customer demographics
 */
@RestController
@CrossOrigin
@RequestMapping("/api/customer/demographics")
@Api(value = "Endpoint for managing customers demographics", tags = ApiDocumentationTags.CUSTOMER)
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DemographicsController {

    private CustomerService service;

    @PostMapping()
    @Secured(PreAuthorizeTags.CREATE_UPDATE_DEMOGRAPHICS)
    @ApiOperation(value ="Create customer demographic", tags = {ApiDocumentationTags.CUSTOMER})
    public ResponseDto<Integer> create(@ApiParam("Data transfer object")
                                       @RequestBody CreateorUpdateCustomerDemographicDto dto) {
        return ResponseDto.success(service.createDemograpic(dto.getDescription()));
    }

    @PutMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_DEMOGRAPHICS)
    @ApiOperation(value = "Updates customer demographic with specified id", tags = {ApiDocumentationTags.CUSTOMER})
    public ResponseDto<Void> update(@ApiParam(value = "Id of the demographic to be updated", example = "123") @PathVariable Integer id,
                                    @ApiParam("Data transfer object")
                                    @RequestBody CreateorUpdateCustomerDemographicDto dto) {
        service.updateDemographic(id, dto.getDescription());
        return ResponseDto.success();
    }

    @DeleteMapping("/{id}")
    @Secured(PreAuthorizeTags.CREATE_UPDATE_DEMOGRAPHICS)
    @ApiOperation(value = "Deletes customer demographic with specified id", tags = {ApiDocumentationTags.CUSTOMER})
    public ResponseDto<Void> delete(@ApiParam(value = "Id of the demographic to be deleted", example = "123") @PathVariable Integer id) {
        service.deleteDemographic(id);
        return ResponseDto.success();
    }
}
