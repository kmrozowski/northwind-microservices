package pl.agh.bdn2019.order.model;

import lombok.*;
import pl.agh.bdn2019.common.model.EntityWithID;
import pl.agh.bdn2019.common.types.Address;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

/**
 * Order entity
 */
@Entity
@Table(name = "Orders")
@AttributeOverride(name = "id", column = @Column(name="OrderID"))
@Getter
@Setter(value = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Order extends EntityWithID<Integer> {

    @Column(name = "CustomerID")
    private Integer customerId;

    @Column(name = "EmployeeID")
    private Integer employeeId;

    @OneToMany
    @JoinColumn(name = "OrderID")
    @NotEmpty
    private Set<OrderDetails> orderDetails;

    @Column(name = "OrderDate")
    private LocalDate orderDate;

    @Column(name = "RequiredDate")
    private LocalDate requiredDate;

    @Column(name = "ShippedDate")
    private LocalDate shippedDate;

    @Column(name = "Freight")
    private BigDecimal freight;

    @AttributeOverrides({
            @AttributeOverride(name = "address", column = @Column(name="ShipAddress")),
            @AttributeOverride(name = "city", column = @Column(name="ShipCity")),
            @AttributeOverride(name = "region", column = @Column(name="ShipRegion")),
            @AttributeOverride(name = "postalCode", column = @Column(name="ShipPostalCode")),
            @AttributeOverride(name = "country", column = @Column(name="ShipCountry")),
    })
    @Embedded
    private Address shipAddress;

    @ManyToOne
    @JoinColumn(name = "ShipVia")
    private Shipper shipper;

    /**
     * Get total price of the order
     * @return
     */
    public BigDecimal totalPrice() {
        return orderDetails.stream()
                .map(OrderDetails::totalPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
