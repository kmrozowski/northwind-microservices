package pl.agh.bdn2019.product.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data transfer object for product category.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(description = "Product category object")
public class CategoryDto implements Serializable {
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 5830289149134660052L;

	@ApiModelProperty(notes = "Entity id (Required)")
	@NotNull
	private Integer id;
	
	@ApiModelProperty(notes = "Category name (Required)")
	@NotEmpty
	private String name;
	
	@ApiModelProperty(notes = "Category description (Optional)")
	private String description;
	
	@ApiModelProperty(notes = "Category image (Optional)")
	private byte[] image;
}
