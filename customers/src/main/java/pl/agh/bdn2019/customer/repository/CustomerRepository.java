package pl.agh.bdn2019.customer.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.agh.bdn2019.customer.dto.exception.CustomerNotFoundException;
import pl.agh.bdn2019.customer.model.Customer;

/**
 * Customer DAO
 */
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer> {

    /**
     * Gets customer by id or throw exception if it does not exists.
     * @param id Entity id
     * @return product category
     */
    default Customer getById(Integer id) {
        return findById(id).orElseThrow(() -> new CustomerNotFoundException(id));
    }
}
