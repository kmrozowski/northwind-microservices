package pl.agh.bdn2019.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;
import pl.agh.bdn2019.common.model.EntityWithID;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * User role entity
 */
@Entity
@Table(name = "Roles")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name="RoleID"))
})
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Role extends EntityWithID<Integer> {

    @Column(name = "RoleName")
    @Enumerated(EnumType.STRING)
    @NaturalId
    private RoleName role;

    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<User> userList = new HashSet<>();

    public boolean isAdminRole() {
        return role == RoleName.ADMIN;
    }
}
