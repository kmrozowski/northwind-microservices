package pl.agh.bdn2019.customer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.agh.bdn2019.common.dto.ResponseDto;
import pl.agh.bdn2019.customer.dto.CustomerDto;

@FeignClient("customers-client")
public interface CustomerClient {

    @GetMapping("/{id}")
    ResponseDto<CustomerDto> get(@PathVariable Integer id);
}
